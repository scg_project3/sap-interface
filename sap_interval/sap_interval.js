const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const chokidar = require('chokidar');

// Connection URL
// const url = 'mongodb://localhost:27017';
const url = 'mongodb://scg:scg@ds239587.mlab.com:39587';
// Database Name
const dbName = 'asrs';

mongoose.connect(url + "/" + dbName, {useMongoClient: true});
mongoose.Promise = global.Promise;

let sapItemMasterSchema = mongoose.Schema({
    material_number: String,
    material_description: String,
    material_group: String,
    base_unit_of_measure: String,
    secondary_unit_of_measure: String,
    weight_per_bag: String,
    storage_type: String,
    package_type: String,
    deletion_flag: String,
    sync: Number,
    update_time: Date
});

let SapItemMaster = mongoose.model('sap_item_master', sapItemMasterSchema);

let sapTransferOperate = mongoose.Schema({
    transfer_order_number: String,
    movement_type: String,
    transfer_order_item: String,
    material_number: String,
    batch_no: String,
    sales_and_distribution_document_number: String,
    weight: String,
    destination_storage_unit_number: String,
    ship_to: String,
    sync: Number,
    update_time: Date
});

let SapTransferOperate = mongoose.model('sap_transfer_operate', sapTransferOperate);

let sapRecentUpdateSchema = mongoose.Schema({
    type: String,
    count: Number,
    update: Date
});

let SapRecentUpdate = mongoose.model('sap_recent_update', sapRecentUpdateSchema);

let ItemMasterSchema = mongoose.Schema({
    _id: String,
    company_id: Number,
    item_code: String,
    item_name: String,
    item_type: String,
    entering_qty: Number,
    description: String,
    moving_type: String,
    primary_uom: String,
    secondary_uom: String,
    itemmaster_color: String,
    itemmaster_id: Number,
    active: Boolean,
    flag: String
}, {collection: 'itemmaster'});

ItemMasterSchema.pre('save', function (next) {
    this._id = this._id.toString();
    next();
});

let ItemMaster = mongoose.model('itemmaster', ItemMasterSchema);

let archiveOrderSchema = mongoose.Schema({
    _id: String,
    transfer_order_no: String,
    status_flag: String,
    movement_type: String,
    transfer_order_item: String,
    item_code: String,
    lot_no: String,
    delivery_order_no: String,
    weight: String,
    lpn_no: String,
    ship_to: String,
    reject_reason: String,
    actual_weights: String,
    regist_date: String,
    archive_date: String,
    archive_pname: String,
    user_id: Number,
    transferorder_id: Number,
    sap_exported: Number
}, {collection: 'archiveorder'});

let archiveOrder = mongoose.model('archiveorder', archiveOrderSchema);

let transferOrderSchema = mongoose.Schema({
    _id: String,
    transfer_order_no: String,
    status_flag: String,
    movement_type: String,
    transfer_order_item: String,
    item_code: String,
    lot_no: String,
    delivery_order_no: String,
    weight: Number,
    lpn_no: String,
    ship_to: String,
    reject_reason: String,
    actual_weights: String,
    regist_date: String,
    archive_date: String,
    archive_pname: String,
    user_id: Number,
    transferorder_id: Number,
    flag: String
}, {collection: 'transferorder'});

transferOrderSchema.pre('save', function (next) {
    this._id = this._id.toString();
    next();
});

let transferOrder = mongoose.model('transferorder', transferOrderSchema);
const archiveWord = 'archive';

const masterCode = 'MST101';
const operationCode = 'TM201';

let startInterface = (file) => {
    // Income
    let arraySplitPath = file.split('/');
    let fileName = arraySplitPath[arraySplitPath.length - 1];
    let companyCode = fileName.substring(0, 3);
    let processCode = fileName.substring(3, 9);
    let fullPathFileIncome = file;
    fs.readFile(fullPathFileIncome, 'utf8', function (err, data) {
        if (err) throw err;
        let dataArray = data.split('\n');
        for (let i in dataArray) {
            dataArray[i] = dataArray[i].split('\t');
            if (dataArray[i].length < 2) {
                dataArray.splice(i, 1);
            }
        }

        for (let index in dataArray) {
            verify(companyCode, processCode, dataArray[index]);
        }

        // move file to archive
        if (fs.existsSync(fullPathFileIncome)) {
            moveFile(fullPathFileIncome, incomeDir + "_" + archiveWord + "/");
        }
    });
};

//moves the $file to $dir2
let moveFile = (file, dir2) => {
    //gets file name and adds it to dir2
    let f = path.basename(file);
    let dest = path.resolve(dir2, f);

    // console.log(file, dest);

    fs.rename(file, dest, (err) => {
        if (err) throw err;
        // else console.log(file + " ==> " + dest);
    });
};

let updated = {};

let verify = (companyCode, processCode, data) => {

    if (processCode.indexOf(masterCode) > -1) {
        updated[masterCode] = 0;
        let itemMaster = {
            "material_number": data[0],
            "material_description": data[1],
            "material_group": data[2],
            "base_unit_of_measure": data[3],
            "secondary_unit_of_measure": data[4],
            "weight_per_bag": data[5],
            "storage_type": data[6],
            "package_type": data[7],
            "deletion_flag": data[8],
            "sync": 0,
            "update_time": Date.now()
        };

        let query = {"material_number": data[0]};
        SapItemMaster.findOneAndUpdate(query, itemMaster, {upsert: true}, function (err, doc) {
            SapItemMaster.findOne(query, function (errSAPItemMaster, docSAPItemMaster) {
                if (docSAPItemMaster) {
                    updated[masterCode]++;
                    let updateObject = {
                        type: 'sap_item_master',
                        count: updated[masterCode],
                        update: Date.now()
                    };
                    SapRecentUpdate.findOneAndUpdate({type: 'sap_item_master'}, updateObject, {upsert: true}, function (err, doc) {
                    });
                    syncCollection(docSAPItemMaster, 'master');
                }
            });
        });

    } else if (processCode.indexOf(operationCode) > -1) {
        updated[operationCode] = 0;
        let itemOperate = {
            transfer_order_number: data[0],
            movement_type: data[1],
            transfer_order_item: data[2],
            material_number: data[3],
            batch_no: data[4],
            sales_and_distribution_document_number: data[5],
            weight: data[6],
            destination_storage_unit_number: data[7],
            ship_to: data[8],
            sync: 0,
            update_time: Date.now()
        };
        let query = {transfer_order_number: data[0], transfer_order_item: data[2]};

        SapTransferOperate.findOneAndUpdate(query, itemOperate, {upsert: true}, function (err, doc) {
            SapTransferOperate.findOne(query, function (errSAPTransferOperate, docSAPTransferOperate) {
                updated[operationCode]++;
                let updateObject = {
                    type: 'sap_transfer_operate',
                    count: updated[operationCode],
                    update: Date.now(),
                };
                SapRecentUpdate.findOneAndUpdate({type: 'sap_transfer_operate'}, updateObject, {upsert: true}, function (err, doc) {
                });
                syncCollection(docSAPTransferOperate, 'transfer_operate');
            });
        });
    }

};

let syncCollection = (sapObject, type) => {
    if (type === 'master') {
        ItemMaster.findOne({"item_code": sapObject.material_number}, function (err, doc) {
            if (doc === null) {
                let newItemMaster = new ItemMaster({
                    "_id": mongoose.Types.ObjectId(),
                    "company_id": 2,
                    "item_code": sapObject.material_number,
                    "item_name": sapObject.material_description,
                    "item_type": sapObject.material_group,
                    "entering_qty": sapObject.weight_per_bag,
                    "description": "",
                    "moving_type": sapObject.storage_type,
                    "primary_uom": sapObject.base_unit_of_measure,
                    "secondary_uom": sapObject.secondary_unit_of_measure,
                    "itemmaster_color": "",
                    "itemmaster_id": 0,
                    "active": true,
                    "flag": "SAP"
                });
                newItemMaster.save(function (err, result) {
                    let materialOutcome = {
                        material_number: sapObject.material_number,
                        material_description: sapObject.material_description,
                        status: 'A',
                        reject_reason: ''
                    };
                    if (err) {
                        materialOutcome.status = 'R';
                        materialOutcome.reject_reason = err;
                    } else {
                        SapItemMaster.findById(sapObject._id, function (err, sapItemMaster) {
                            sapItemMaster.sync = 1;
                            sapItemMaster.save();
                        });
                    }
                    exportMaterial(materialOutcome);
                });
            }
        });
    } else if (type === 'transfer_operate') {
        let query = {
            transfer_order_no: sapObject.transfer_order_number,
            transfer_order_item: sapObject.transfer_order_item
        };
        transferOrder.findOne(query, function (err, doc) {
            let newTransferOrder;
            if (doc === null) {
                newTransferOrder = new transferOrder({
                    "_id": mongoose.Types.ObjectId(),
                    "transfer_order_no": sapObject.transfer_order_number,
                    "status_flag": "0",
                    "movement_type": sapObject.movement_type,
                    "transfer_order_item": sapObject.transfer_order_item,
                    "item_code": sapObject.material_number,
                    "lot_no": sapObject.batch_no,
                    "delivery_order_no": "",
                    "weight": sapObject.weight,
                    "lpn_no": sapObject.sales_and_distribution_document_number,
                    "ship_to": sapObject.ship_to,
                    "reject_reason": "",
                    "actual_weights": "0",
                    "regist_date": Date.now(),
                    "archive_date": Date.now(),
                    "archive_pname": "",
                    "user_id": 0,
                    "transferorder_id": 0,
                    "flag": "SAP"
                });

            } else {
                newTransferOrder = doc;
                newTransferOrder.movement_type = sapObject.movement_type;
                newTransferOrder.item_code = sapObject.material_number;
                newTransferOrder.lot_no = sapObject.batch_no;
                newTransferOrder.weight = sapObject.weight;
                newTransferOrder.lpn_no = sapObject.sales_and_distribution_document_number;
                newTransferOrder.ship_to = sapObject.ship_to;
                newTransferOrder.archive_date = Date.now();
            }
            newTransferOrder.save(function (err, result) {
                if (result) {
                    SapTransferOperate.findById(sapObject._id, function (err, sapTransferOperate) {
                        sapTransferOperate.sync = 1;
                        sapTransferOperate.save();
                    });
                }
            });
        });
    }
};

let getCompletedOrder = () => {

    archiveOrder.find({sap_exported: {$ne: 1}}, function (err, docs) {
        if (!err) {
            if (docs.length > 0) {
                exportOrders(docs);
            }
        }

    });

};

let exportMaterial = (materialOutcome) => {
    // Outcome Material txt
    let text = '';
    text += materialOutcome.material_number + "\t";
    text += materialOutcome.material_description + "\t";
    text += materialOutcome.status + "\t";
    text += materialOutcome.reject_reason;

    let currentTime = new Date();
    let fileName = "GSCMST111" + currentTime.getFullYear();
    fileName += ('0' + (currentTime.getMonth() + 1)).slice(-2);
    fileName += ('0' + (currentTime.getDate())).slice(-2);
    fileName += ('0' + (currentTime.getHours())).slice(-2);
    fileName += ('0' + (currentTime.getMinutes())).slice(-2);
    fileName += ('0' + (currentTime.getSeconds())).slice(-2);
    fileName += ".txt";

    fs.exists(outcomeDir + "/" + fileName, (exists) => {
        if (exists) {
            console.log('again');
            setTimeout(() => {
                exportMaterial(materialOutcome);
            }, 3000);
        } else {
            fs.writeFile(outcomeDir + "/" + fileName, text, function (err) {
                if (!err) {
                    console.log("The file was saved!");
                }
            });
        }
    });

};

let exportOrders = (docs) => {
    let text = '';
    for (let d in docs) {
        if (d != 0) {
            text += "\n";
        }
        let doc = docs[d];
        text += doc.transfer_order_no + "\t";
        text += doc.movement_type + "\t";
        text += doc.item_code + "\t";
        text += doc.transfer_order_item + "\t";
        text += doc.lot_no + "\t";
        text += doc.delivery_order_no + "\t";
        text += doc.lpn_no + "\t";
        text += "A\t";
        text += doc.reject_reason
    }

    let currentTime = new Date();
    let fileName = "GSCMST211" + currentTime.getFullYear();
    fileName += ('0' + (currentTime.getMonth() + 1)).slice(-2);
    fileName += ('0' + (currentTime.getDate())).slice(-2);
    fileName += ('0' + (currentTime.getHours())).slice(-2);
    fileName += ('0' + (currentTime.getMinutes())).slice(-2);
    fileName += ('0' + (currentTime.getSeconds())).slice(-2);
    fileName += ".txt";
    fs.writeFile(outcomeDir + "/" + fileName, text, function (err) {
        if (!err) {
            console.log("The file was saved!");
            for (let d in docs) {
                let doc = docs[d];
                updateTO(doc);
            }
        }
    });
};

let updateTO = (doc) => {
    let id = doc._id;
    delete doc._id;
    doc.sap_exported = 1;
    archiveOrder.findOneAndUpdate({_id: id}, doc, function (err, result) {

    });
};

setInterval(() => {
    console.log(new Date());
    getCompletedOrder();
}, 300000);

let parentDir = path.dirname(__dirname) + '/app/server/sap_data';
let incomeDir = parentDir + '/income';
let outcomeDir = parentDir + '/outcome';
if (!fs.existsSync(parentDir)) {
    fs.mkdirSync(parentDir);
}
if (!fs.existsSync(incomeDir)) {
    fs.mkdirSync(incomeDir);
}
if (!fs.existsSync(incomeDir + "_" + archiveWord)) {
    fs.mkdirSync(incomeDir + "_" + archiveWord);
}
if (!fs.existsSync(outcomeDir)) {
    fs.mkdirSync(outcomeDir);
}
if (!fs.existsSync(outcomeDir + "_" + archiveWord)) {
    fs.mkdirSync(outcomeDir + "_" + archiveWord);
}

// One-liner for current directory, ignores .dotfiles
chokidar.watch(incomeDir, {ignored: /(^|[\/\\])\../}).on('all', (event, path) => {
    if (event === 'add') {
        console.log(path);
        startInterface(path);
    }
});

