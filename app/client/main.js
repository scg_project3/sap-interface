import 'bootstrap/dist/css/bootstrap.css';
import Chart from 'chart.js';

import dataTablesBootstrap from 'datatables.net-bs';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
dataTablesBootstrap(window, $);

require('bootstrap');
Modal.allowMultiple = true;