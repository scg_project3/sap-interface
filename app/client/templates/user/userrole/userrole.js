/*****************************************************************************/
/* Userrole: Event Handlers */
/*****************************************************************************/
Template.Userrole.events({
    'click #create': function() {
        clearInput("detailForm");
        $("#saveModal").attr('mode', 'add');
        $("#modalManage").modal('show');
    },
    'click #btn_edit': function(e) {
        var id = $(e.currentTarget).attr('data_id');
        var data = Roles.findOne({ role_id: parseInt(id) });
        arrayToInput('detailForm', data);
        $("#saveModal").attr('mode', 'edit');
        $("#modalManage").modal('show');
        $("#saveModal").attr('data_id', id);
    },
    'click #saveModal': function(e) {

        var data = inputToArray('detailForm');
        var mode = $(e.currentTarget).attr('mode');
        if (mode == 'add') {
            Meteor.call('insert_role', data, function(error, result) {
                $("#modalManage").modal('hide');
                Modal.hide('NmOneLoading');
            });
        } else {
            data.role_id = parseInt($(e.currentTarget).attr('data_id'));
            Meteor.call('update_role', data, function(error, result) {
                $("#modalManage").modal('hide');
                Modal.hide('NmOneLoading');
            });
        }
    },
    'keyup #input_search': function(e) {
        var key = $(e.target).val();
        $('.dataTables_filter input').val(key);
        $('.dataTables_filter input').keyup();
    },
});

/*****************************************************************************/
/* Userrole: Helpers */
/*****************************************************************************/
Template.Userrole.helpers({
});

/*****************************************************************************/
/* Userrole: Lifecycle Hooks */
/*****************************************************************************/
Template.Userrole.onCreated(function () {
});

Template.Userrole.onRendered(function () {
});

Template.Userrole.onDestroyed(function () {
});
