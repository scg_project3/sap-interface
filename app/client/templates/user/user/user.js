/*****************************************************************************/
/* User: Event Handlers */
/*****************************************************************************/
TempGroupandRole = new Mongo.Collection(null);
Template.User.events({
	'click #create': function(e) {
        $(".form-password").show();
        $("#myModalLabel").text("Create User");
        $(".changepass").hide();
        clearInput("registerForm");
        $("#modalUser").modal('show');
        $("#saveUser").attr("data-check", "insertusers");
        $(".registerForm[field=username]").attr('disabled', false);
        $(".registerForm[field=password]").attr('disabled', false);
        $(".registerForm[field=password_confirm]").attr('disabled', false);
    },
    'click #saveUser': function(e) {
        var datausers = inputToArray("registerForm");
        datausers.role_id = parseInt(datausers.role_id);
        var check = $(e.target).attr('data-check');

        if (datausers.username == "" && datausers.password == "" && datausers.password_confirm == "" || datausers.email == "" || datausers.firstname == "" || datausers.lastname == "" || datausers.tel_number == "") {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน');
            return false;
        }
        if (check == "insertusers") {
            Meteor.call('insertusers', datausers, function(error, result) {
                if (error) {
                    alert('เกิดข้อผิดพลาด');
                } else {
                    $('#modalUser').modal('hide');
                }

            });
        } else {
            datausers.user_id = parseInt($(e.target).attr('user_id'));
            Meteor.call('updateusers', datausers, function(error, result) {
                if (error) {
                    alert('เกิดข้อผิดพลาด');
                } else {
                    $('#modalUser').modal('hide');
                }
            });
        }

    },
    'click #btnEditUser': function(e) {
        $(".form-password").show();
        $("#myModalLabel").text("Edit User");
        $(".changepass").show();
        $("#modalUser").modal('show');
        $("#saveUser").attr("data-check", "updateusers");
        $(".changepass").show();
        $(".form-password").hide();
        var user_id = parseInt($(e.target).attr("user_id"));
        console.log('d', user_id)
        var query_users = Meteor.users.findOne({ "profile.user_id": user_id });
        console.log('a', query_users)
        arrayToInput('registerForm', query_users.profile);
        $("#saveUser").attr("user_id", user_id);
        $(".registerForm[field=password]").attr('disabled', true);
        $(".registerForm[field=password_confirm]").attr('disabled', true);
        $(".registerForm[field=username]").attr('disabled', true);
    },
    'click #openChangePassword': function(e) {
        $("#modalChangePassword").modal('show');
        $("#modalUser").modal('hide');
        clearInput("changepass");
    },
    'click #savePassword': function(e) {
        var datachangepass = inputToArray("changepass");
        var datauser = inputToArray("registerForm");

        if (datachangepass.password == datachangepass.password_confirm) {
            var user = Meteor.users.findOne({
                username: datauser.username
            });
            datachangepass.user_id = user._id
            Meteor.call('setPassword', datachangepass, function(error, result) {
                if (error) {
                    sAlert.warning(error.reason);
                } else {
                    $("#modalChangePassword").modal('hide');
                }
            });
        } else {
            sAlert.warning("password not match");
        }
    },
    'keyup #input_search': function(e) {
        var key = $(e.target).val();
        $('.dataTables_filter input').val(key);
        $('.dataTables_filter input').keyup();
    },
    "click .btnDeleteuser": function(e) {
        var datausers = inputToArray("registerForm");
        console.log('datausers',datausers)
        datausers.user_id = parseInt($(e.target).attr('user_id'));
        bootbox.confirm({
            title: "Confirm Dialog",
            message: "คุณต้องการลบหรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    Meteor.call('updatedeleteuser', datausers, function(error, result) {
                        if (error) {
                            alert('เกิดข้อผิดพลาด');
                        } else {
                            $('#modalUser').modal('hide');
                        }
                    })
                }
            }
        });
    },
});

/*****************************************************************************/
/* User: Helpers */
/*****************************************************************************/
Template.User.helpers({
    'getuserrole': function() {
        return Roles.find().fetch();
    },
    'filterstatus': function() {
        return {"profile.status": 1 }
    },
});

/*****************************************************************************/
/* User: Lifecycle Hooks */
/*****************************************************************************/
Template.User.onCreated(function () {
    Meteor.subscribe('roles');
});

Template.User.onRendered(function () {
});

Template.User.onDestroyed(function () {
});
