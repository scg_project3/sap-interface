// Meteor.autorun(function() {
//     Session.set('session_user_profile', reqUserProfile());
//     console.log(Session.get('session_user_profile'));
// });

/*****************************************************************************/
/* LayoutNav: Event Handlers */
/*****************************************************************************/
Template.LayoutNav.events({
    'click .menu-collapse': function(event) {
        if ($("#" + event.currentTarget.id + " .sub-menu-collapse").hasClass('collapse') == true) {
            $(".menu-collapse").removeClass('active-link');
            $(".menu-collapse .sub-menu-collapse").addClass('collapse');
            $("#" + event.currentTarget.id + " .sub-menu-collapse").removeClass('collapse');
            $("#" + event.currentTarget.id).addClass('active-link');
            $("#mainnav-container").removeClass('nav-collapse');
            $("#footer").removeClass('nav-content-collapse');
            $(".navbar-brand").removeClass('nav-collapse');
            $(".menu-collapse .menu-title").removeClass('nav-title-collapse');
            $(".navbar-content").removeClass('nav-content-collapse');
            $("#content-container").removeClass('nav-content-collapse');
            $(".content-container").removeClass('nav-content-collapse');
            $(".menu-collapse i.arrow").removeClass('nav-title-collapse');
            $(".menu-collapse i.fa").removeClass('nav-icon-collapse');
        } else {
            $("#" + event.currentTarget.id + " .sub-menu-collapse").addClass('collapse');
            $("#" + event.currentTarget.id).removeClass('active-link');
        }
    },
    'click .nav-menu-link': function(event) {
        // Session.set('session_get_url', Router.current().route.path(this));
    },    
});

/*****************************************************************************/
/* LayoutNav: Helpers */
/*****************************************************************************/
Template.LayoutNav.helpers({
    // 'activeMenu': function() {
    //     $('a[href="' + Session.get('session_get_url') + '"]').addClass('active');
    //     $('a[href="' + Session.get('session_get_url') + '"]').parents().eq(2).addClass('active-link');
    //     $('a[href="' + Session.get('session_get_url') + '"]').parents().eq(1).removeClass('collapse');
    // },
    'check_systemlayout': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 2) {
            a = true
        }
        return a;
    },
    'check_storageprofile': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 2) {
            a = true
        }
        return a;
    },
    'check_itemprofile': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 2) {
            a = true
        }
        return a;
    },
    'check_packagetype': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 2) {
            a = true
        }
        return a;
    },
    'check_pallet': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 2) {
            a = true
        }
        return a;
    },
    'check_user': function(role_id) {
        var a = false
        if (role_id == 1) {
            a = true
        }
        return a;
    },
    'check_userrole': function(role_id) {
        var a = false
        if (role_id == 1) {
            a = true
        }
        return a;
    },
    'check_lotlist': function(role_id) {
        var a = false
        if (role_id == 1 || role_id == 4 || role_id == 3) {
            a = true
        }
        return a;
    },
});

/*****************************************************************************/
/* LayoutNav: Lifecycle Hooks */
/*****************************************************************************/
Template.LayoutNav.onCreated(function() {});

Template.LayoutNav.onRendered(function() {
    Session.set('session_get_url', Router.current().route.path(this));
});

Template.LayoutNav.onDestroyed(function() {});

/*****************************************************************************/
/* LayoutHeader: Event Handlers */
/*****************************************************************************/
Template.LayoutHeader.events({
    'click #btnLogout': function() {
        Meteor.logout();
    },
    'click .tgl-menu-btn': function(event) {
        event.preventDefault();
        event.stopPropagation();
        if ($("#mainnav-container").hasClass('nav-collapse') == false) {
            $("#mainnav-container").addClass('nav-collapse');
            $(".navbar-brand").addClass('nav-collapse');
            $(".navbar-content").addClass('nav-content-collapse');
            $("#content-container").addClass('nav-content-collapse');
            $(".content-container").addClass('nav-content-collapse');
            $("#footer").addClass('nav-content-collapse');
            $(".menu-collapse .menu-title").addClass('nav-title-collapse');
            $(".menu-collapse i.arrow").addClass('nav-title-collapse');
            $(".menu-collapse i.fa").addClass('nav-icon-collapse');
            $(".menu-collapse .sub-menu-collapse").addClass('collapse');
        } else {
            $("#mainnav-container").removeClass('nav-collapse');
            $(".navbar-brand").removeClass('nav-collapse');
            $(".navbar-content").removeClass('nav-content-collapse');
            $("#content-container").removeClass('nav-content-collapse');
            $(".content-container").removeClass('nav-content-collapse');
            $("#footer").removeClass('nav-content-collapse');
            $(".menu-collapse .menu-title").removeClass('nav-title-collapse');
            $(".menu-collapse i.arrow").removeClass('nav-title-collapse');
            $(".menu-collapse i.fa").removeClass('nav-icon-collapse');
        }
    },
    'click #btnAuthorizationSetup': function() {
        // Modal.show('AuthorizationSetup');
        // Router.go("/authorization");
    }
});

/*****************************************************************************/
/* LayoutHeader: Helpers */
/*****************************************************************************/
Template.LayoutHeader.helpers({});

/*****************************************************************************/
/* LayoutHeader: Lifecycle Hooks */
/*****************************************************************************/
Template.LayoutHeader.onCreated(function() {
    Meteor.subscribe('conf_currency');
    Meteor.subscribe('conf_jobtype');
    Meteor.subscribe('conf_pricetype');
    Meteor.subscribe('master_customer');
    Meteor.subscribe('master_marketing');
    Meteor.subscribe('storing');
    Meteor.subscribe('itemprofile');
    Meteor.subscribe('storageprofile');
});

Template.LayoutHeader.onRendered(function() {});

Template.LayoutHeader.onDestroyed(function() {});