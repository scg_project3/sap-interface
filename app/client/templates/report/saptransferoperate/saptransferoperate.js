/*****************************************************************************/
/* transferOperate: Event Handlers */
/*****************************************************************************/
Template.transferOperate.events({
    'keyup #input_search': () => {
        let key = $(event.target).val();
        $('.dataTables_filter input').val(key);
        $('.dataTables_filter input').keyup();
    },

    'click .btnViewSapTransferOperate': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
        $("#modalSapTransferOperateTitle").text("Transfer Order Number #" + title);
        arrayToInput('sapTransferOperateForm', query);
        $("#modalSapTransferOperate").modal('show');
    },

    'click .btnSyncSapTransferOperate': (e) => {
        e.preventDefault();
        let id = $(e.currentTarget).attr("data-id");
        $("#syncTransferOperateBtn").attr("data-id", id);
        let title = $(e.currentTarget).attr("data-title");
        $("#modalSAPTransferOperateCompareTitle").text("#" + title);
        $("#modalSAPTransferOperateCompare td").text("").removeClass("btn-warning");

        let transferOrderNo = title.split("-")[0];
        let transferOrderItem = title.split("-")[1];

        let sapTransferOperate = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
        let transferOrder = Transferorder.findOne({
            "transfer_order_no": transferOrderNo,
            "transfer_order_item": transferOrderItem
        });

        if (sapTransferOperate && transferOrder) {
            for (let i in transferOrder) {
                if ($("#modalSAPTransferOperateCompare td#compare1_" + i)) {
                    if (i === "weight") {
                        transferOrder[i] = parseFloat(transferOrder[i]);
                    }
                    $("#modalSAPTransferOperateCompare td#compare1_" + i).text(transferOrder[i]);
                }
            }
            for (let j in sapTransferOperate) {
                if ($("#modalSAPTransferOperateCompare td#compare2_" + j)) {
                    if (j === "weight") {
                        sapTransferOperate[j] = parseFloat(sapTransferOperate[j]);
                    }
                    $("#modalSAPTransferOperateCompare td#compare2_" + j).text(sapTransferOperate[j]);
                }
            }
            $("#modalSAPTransferOperateCompare tr").each(function (index, element) {
                if ($(element).children()[1].tagName === 'TD') {
                    if ($(element).children()[1].textContent.trim() !== $(element).children()[2].textContent.trim()) {
                        $($(element).children()[2]).addClass('btn-warning');
                    }
                }
            });
            $("#modalSAPTransferOperateCompare").modal("show");
        } else {
            alert("มีบางอย่างผิดพลาด");
        }

    },

    'click #syncTransferOperateBtn': (e) => {
        let r = confirm("Confirm?");
        if (r === true) {
            let id = $(e.currentTarget).attr("data-id");
            let sapTransferOperate = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
            let query = {
                "transfer_order_no": sapTransferOperate.transfer_order_number,
                "transfer_order_item": sapTransferOperate.transfer_order_item
            };
            let data = {
                "status_flag" : "0",
                "movement_type" : sapTransferOperate.movement_type,
                "item_code" : sapTransferOperate.material_number,
                "lot_no" : sapTransferOperate.batch_no,
                "delivery_order_no" : "",
                "weight" : sapTransferOperate.weight,
                "lpn_no" : sapTransferOperate.sales_and_distribution_document_number,
                "ship_to" :sapTransferOperate.ship_to,
                "reject_reason" : "",
                "actual_weights" : "0",
                "regist_date" : Date.now(),
                "archive_date" : Date.now(),
                "archive_pname" : "",
                "user_id" : 0,
                "transferorder_id" : 0,
                "flag": "SAP"
            };

            Meteor.call("updateTransferorder", query, data, function (err, response) {
                Meteor.call("updateSAPTransferOperate", sapTransferOperate._id, {"sync": 1}, function (err2, response2) {
                });
            });
            $("#modalSAPTransferOperateCompare").modal('hide');
        }
    },
});

/*****************************************************************************/
/* transferOperate: Helpers */
/*****************************************************************************/
Template.transferOperate.helpers({

});

/*****************************************************************************/
/* transferOperate: Lifecycle Hooks */
/*****************************************************************************/
Template.transferOperate.onCreated(function () {
    Meteor.subscribe('sap_transfer_operate');
    Meteor.subscribe('transferorder');
});

Template.transferOperate.onRendered(function () {

});

Template.transferOperate.onDestroyed(function () {

});