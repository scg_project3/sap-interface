/*****************************************************************************/
/* exportTransferOrder: Event Handlers */
/*****************************************************************************/
Template.exportTransferOrder.events({
    'keyup #input_search': () => {
        let key = $(event.target).val();
        $('.dataTables_filter input').val(key);
        $('.dataTables_filter input').keyup();
    },

    'click .btnViewExportTransferOrder': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = ArchiveOrder.findOne({"_id": id});
        $("#modalExportTransferOrderTitle").text("LPN No./SU #" + title);
        arrayToInput('exportTransferOrderForm', query);
        $("#modalExportTransferOrder").modal('show');
    },

});

/*****************************************************************************/
/* exportTransferOrder: Helpers */
/*****************************************************************************/
Template.exportTransferOrder.helpers({
    selector:function (){
        return { sap_exported : 1};
    }
});

/*****************************************************************************/
/* exportTransferOrder: Lifecycle Hooks */
/*****************************************************************************/
Template.exportTransferOrder.onCreated(function () {
    Meteor.subscribe('sap_transfer_operate');
});

Template.exportTransferOrder.onRendered(function () {

});

Template.exportTransferOrder.onDestroyed(function () {

});