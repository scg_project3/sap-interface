/*****************************************************************************/
/* masterItem: Event Handlers */
/*****************************************************************************/
Template.masterItem.events({
    'click .btnViewSapItemMaster': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
        $("#modalSapMasterItemTitle").text("Material Number #" + title);
        arrayToInput('sapMasterForm', query);
        $("#modalSapMasterItem").modal('show');
    },

    'keyup #input_search': () => {
        let key = $(event.target).val();
        $('.dataTables_filter input').val(key);
        $('.dataTables_filter input').keyup();
    },

    'change #syncStatus': (e) => {
        Session.set('sync_status', $(e.currentTarget).val());

    },

    'click .btnSyncSapItemMaster': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        $("#syncItemMaserBtn").attr("data-id", id);
        let title = $(e.currentTarget).attr('data-title');
        $("#modalSapMasterItemCompareTitle").text("Material Number #" + title);
        $("#modalSapMasterItemCompare td").text("").removeClass("btn-warning");

        let sapItemMaster = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
        let itemMaster = ItemMaster.findOne({"item_code": sapItemMaster.material_number});
        for (let i in sapItemMaster) {
            if ($("#modalSapMasterItemCompare td#compare_" + i)) {
                $("#modalSapMasterItemCompare td#compare_" + i).text(sapItemMaster[i]);
            }
        }
        for (let j in itemMaster) {
            if ($("#modalSapMasterItemCompare td#compare_" + j)) {
                $("#modalSapMasterItemCompare td#compare_" + j).text(itemMaster[j]);
            }
        }
        $("#modalSapMasterItemCompare tr").each(function (index, element) {
            if ($(element).children()[1].tagName === 'TD') {
                if ($(element).children()[1].textContent.trim() !== $(element).children()[2].textContent.trim()) {
                    $($(element).children()[2]).addClass('btn-warning');
                }
            }
        });
        $("#modalSapMasterItemCompare").modal('show');
    },

    'click #syncItemMaserBtn': (e) => {

        let r = confirm("Confirm?");
        if (r === true) {
            let id = $(e.currentTarget).attr("data-id");
            let sapItemMaster = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
            let data = {
                "item_name": sapItemMaster.material_description,
                "item_type": sapItemMaster.material_group,
                "entering_qty": parseInt(sapItemMaster.weight_per_bag),
                "moving_type": sapItemMaster.storage_type,
                "primary_uom": sapItemMaster.base_unit_of_measure,
                "secondary_uom": sapItemMaster.secondary_unit_of_measure,
                "flag": "SAP"
            };

            Meteor.call("updateItemMaster", sapItemMaster.material_number, data, function (err, response) {
                Meteor.call("updateSAPItemMaster", sapItemMaster._id, {"sync": 1}, function (err2, response2) {
                });
            });
            $("#modalSapMasterItemCompare").modal('hide');
        }
    }
});

/*****************************************************************************/
/* masterItem: Helpers */
/*****************************************************************************/
Template.masterItem.helpers({
    selector: function () {
        let querySync = {sync: {$ne: -1}};
        let valSync = Session.get('sync_status');
        if (parseInt(valSync) !== -1) {
            querySync = {sync: parseInt(valSync)};
        }
        return querySync;
    }
});

/*****************************************************************************/
/* masterItem: Lifecycle Hooks */
/*****************************************************************************/
Template.masterItem.onCreated(function () {
    Meteor.subscribe('sap_item_master');
    Session.set('sync_status', -1);
});

Template.masterItem.onRendered(function () {

});

Template.masterItem.onDestroyed(function () {

});