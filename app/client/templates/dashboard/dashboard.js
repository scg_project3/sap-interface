/*****************************************************************************/
/* dashboard: Event Handlers */
/*****************************************************************************/
Template.dashboard.events({
    'click .btnViewSapItemMaster': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
        $("#modalSapMasterItemTitle").text("Material Number #" + title);
        arrayToInput('sapMasterForm', query);
        $("#modalSapMasterItem").modal('show');
    },

    'click .btnViewSapTransferOperate': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
        $("#modalSapTransferOperateTitle").text("Transfer Order Number #" + title);
        arrayToInput('sapTransferOperateForm', query);
        $("#modalSapTransferOperate").modal('show');
    },

    'click .btnViewExportTransferOrder': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        let title = $(e.currentTarget).attr('data-title');
        let query = ArchiveOrder.findOne({"_id": id});
        $("#modalExportTransferOrderTitle").text("LPN No./SU #" + title);
        arrayToInput('exportTransferOrderForm', query);
        $("#modalExportTransferOrder").modal('show');
    },

    'click .btnSyncSapItemMaster': (e) => {
        let id = $(e.currentTarget).attr("data-id");
        $("#syncItemMaserBtn").attr("data-id", id);
        let title = $(e.currentTarget).attr('data-title');
        $("#modalSapMasterItemCompareTitle").text("Material Number #" + title);
        $("#modalSapMasterItemCompare td").text("").removeClass("btn-warning");

        let sapItemMaster = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
        let itemMaster = ItemMaster.findOne({"item_code": sapItemMaster.material_number});
        for (let i in sapItemMaster) {
            if ($("#modalSapMasterItemCompare td#compare_" + i)) {
                $("#modalSapMasterItemCompare td#compare_" + i).text(sapItemMaster[i]);
            }
        }
        for (let j in itemMaster) {
            if ($("#modalSapMasterItemCompare td#compare_" + j)) {
                $("#modalSapMasterItemCompare td#compare_" + j).text(itemMaster[j]);
            }
        }
        $("#modalSapMasterItemCompare tr").each(function (index, element) {
            if ($(element).children()[1].tagName === 'TD') {
                if ($(element).children()[1].textContent.trim() !== $(element).children()[2].textContent.trim()) {
                    $($(element).children()[2]).addClass('btn-warning');
                }
            }
        });
        $("#modalSapMasterItemCompare").modal('show');
    },

    'click #syncItemMaserBtn': (e) => {

        let r = confirm("Confirm?");
        if (r === true) {
            let id = $(e.currentTarget).attr("data-id");
            let sapItemMaster = SapItemMaster.findOne({"_id": new Mongo.ObjectID(id)});
            let data = {
                "item_name": sapItemMaster.material_description,
                "item_type": sapItemMaster.material_group,
                "entering_qty": parseInt(sapItemMaster.weight_per_bag),
                "moving_type": sapItemMaster.storage_type,
                "primary_uom": sapItemMaster.base_unit_of_measure,
                "secondary_uom": sapItemMaster.secondary_unit_of_measure,
                "flag": "SAP"
            };

            Meteor.call("updateItemMaster", sapItemMaster.material_number, data, function (err, response) {
                Meteor.call("updateSAPItemMaster", sapItemMaster._id, {"sync": 1}, function (err2, response2) {
                });
            });
            $("#modalSapMasterItemCompare").modal('hide');
        }
    },

    'click .btnSyncSapTransferOperate': (e) => {
        e.preventDefault();
        let id = $(e.currentTarget).attr("data-id");
        $("#syncTransferOperateBtn").attr("data-id", id);
        let title = $(e.currentTarget).attr("data-title");
        $("#modalSAPTransferOperateCompareTitle").text("#" + title);
        $("#modalSAPTransferOperateCompare td").text("").removeClass("btn-warning");

        let transferOrderNo = title.split("-")[0];
        let transferOrderItem = title.split("-")[1];

        let sapTransferOperate = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
        let transferOrder = Transferorder.findOne({
            "transfer_order_no": transferOrderNo,
            "transfer_order_item": transferOrderItem
        });

        if (sapTransferOperate && transferOrder) {
            for (let i in transferOrder) {
                if ($("#modalSAPTransferOperateCompare td#compare1_" + i)) {
                    if (i === "weight") {
                        transferOrder[i] = parseFloat(transferOrder[i]);
                    }
                    $("#modalSAPTransferOperateCompare td#compare1_" + i).text(transferOrder[i]);
                }
            }
            for (let j in sapTransferOperate) {
                if ($("#modalSAPTransferOperateCompare td#compare2_" + j)) {
                    if (j === "weight") {
                        sapTransferOperate[j] = parseFloat(sapTransferOperate[j]);
                    }
                    $("#modalSAPTransferOperateCompare td#compare2_" + j).text(sapTransferOperate[j]);
                }
            }
            $("#modalSAPTransferOperateCompare tr").each(function (index, element) {
                if ($(element).children()[1].tagName === 'TD') {
                    if ($(element).children()[1].textContent.trim() !== $(element).children()[2].textContent.trim()) {
                        $($(element).children()[2]).addClass('btn-warning');
                    }
                }
            });
            $("#modalSAPTransferOperateCompare").modal("show");
        } else {
            alert("มีบางอย่างผิดพลาด");
        }

    },

    'click #syncTransferOperateBtn': (e) => {
        let r = confirm("Confirm?");
        if (r === true) {
            let id = $(e.currentTarget).attr("data-id");
            let sapTransferOperate = SapTransferOperate.findOne({"_id": new Mongo.ObjectID(id)});
            let query = {
                "transfer_order_no": sapTransferOperate.transfer_order_number,
                "transfer_order_item": sapTransferOperate.transfer_order_item
            };
            let data = {
                "status_flag" : "0",
                "movement_type" : sapTransferOperate.movement_type,
                "item_code" : sapTransferOperate.material_number,
                "lot_no" : sapTransferOperate.batch_no,
                "delivery_order_no" : "",
                "weight" : sapTransferOperate.weight,
                "lpn_no" : sapTransferOperate.sales_and_distribution_document_number,
                "ship_to" :sapTransferOperate.ship_to,
                "reject_reason" : "",
                "actual_weights" : "0",
                "regist_date" : Date.now(),
                "archive_date" : Date.now(),
                "archive_pname" : "",
                "user_id" : 0,
                "transferorder_id" : 0,
                "flag": "SAP"
            };

            Meteor.call("updateTransferorder", query, data, function (err, response) {
                Meteor.call("updateSAPTransferOperate", sapTransferOperate._id, {"sync": 1}, function (err2, response2) {
                });
            });
            $("#modalSAPTransferOperateCompare").modal('hide');
        }
    },

});

/*****************************************************************************/
/* dashboard: Helpers */
/*****************************************************************************/
Template.dashboard.helpers({
    lineGrape: () => {
        let masterItems = SapItemMaster.find({"update_time": {$gt: new Date(Date.now() - 24 * 60 * 60 * 1000)}}, {sort: {update_time: 1}}).fetch();
        let transferItem = SapTransferOperate.find({"update_time": {$gt: new Date(Date.now() - 24 * 60 * 60 * 1000)}}, {sort: {update_time: 1}}).fetch();
        let currentHour = new Date().getHours();
        let currentMintues = new Date().getMinutes();
        if (currentMintues > 0) {
            currentHour += 1;
        }

        // Label
        let labels = [];
        let indexLable = {};
        let indexStart = 0;
        for (let i = currentHour + 1; i < 24; i++) {
            labels.push(i.toString());
            indexLable[i] = indexStart;
            indexStart++;
        }
        for (let i = 0; i <= currentHour; i++) {
            labels.push(i.toString());
            indexLable[i] = indexStart;
            indexStart++;
        }

        // masterItem DataSet
        let itemMasterData = Array.apply(null, Array(24)).map(Number.prototype.valueOf, 0);
        for (let m in masterItems) {
            let itemMasterHour = new Date(masterItems[m].update_time).getHours();
            let indexAddValue = parseInt(indexLable[itemMasterHour]);
            itemMasterData[indexAddValue] += 1;
        }

        let masterItemDataSet = {
            label: 'Item Master',
            borderColor: ["#0000FF"],
            backgroundColor: ["#0000FF"],
            borderWidth: 2,
            data: itemMasterData,
            fill: false
        };

        // transferItem DataSet
        let itemTransfer = Array.apply(null, Array(24)).map(Number.prototype.valueOf, 0);
        for (let m in transferItem) {
            let itemTransferHour = new Date(transferItem[m].update_time).getHours();
            let indexAddValue = parseInt(indexLable[itemTransferHour]);
            itemTransfer[indexAddValue] += 1;
        }

        let transferItemDataSet = {
            label: 'Transfer Order',
            borderColor: ["#ff0000"],
            backgroundColor: ["#ff0000"],
            borderWidth: 2,
            data: itemTransfer,
            fill: false
        };

        if (chart !== undefined) {
            chart.data.labels = labels;
            chart.data.datasets[0] = masterItemDataSet;
            chart.data.datasets[1] = transferItemDataSet;
            chart.update();
        }

    },
    selector: function () {
        return {sap_exported: 1};
    }
});

/*****************************************************************************/
/* dashboard: Lifecycle Hooks */
/*****************************************************************************/
Template.dashboard.onCreated(function () {
    Meteor.subscribe('sap_item_master');
    Meteor.subscribe('sap_transfer_operate');
    Meteor.subscribe('sap_recent_update');
    Meteor.subscribe('archiveorder');
    Meteor.subscribe('itemmaster');
    Meteor.subscribe('transferorder');
});

let chart;
Template.dashboard.onRendered(function () {
    let ctx = document.getElementById("DailyChart").getContext('2d');
    chart = new Chart(ctx, {
        type: 'line',
    });

    if ($(".navbar-content").hasClass("nav-content-collapse") === false) {
        $(".mainnav-toggle").click()
    }
});

Template.dashboard.onDestroyed(function () {

});