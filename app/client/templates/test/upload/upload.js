/*****************************************************************************/
/* Upload: Event Handlers */
/*****************************************************************************/
Template.Upload.events({
    "click #submitUpload": (e) => {
        e.preventDefault();
        let files = $("#uploadElement")[0].files;
        if (files.length > 0) {
            $("#fileListArea").hide();
            $("#fileList").children().remove();
            for (let f = 0; f < files.length; f++) {
                $("#fileList").append("<li>" + files[f].name + "</li>");
                updateFile(files[f]);
            }
            $("#fileListArea").show();
        }
    },

    "click #resetData": (e) => {
        Meteor.call('reset-data', function (err, result) {
            if (result == true) {
                alert('Reset completed!!');
            }
        });
    }
});

/*****************************************************************************/
/* Upload: Helpers */
/*****************************************************************************/
Template.Upload.helpers({});

/*****************************************************************************/
/* Upload: Lifecycle Hooks */
/*****************************************************************************/
Template.Upload.onCreated(function () {
});

Template.Upload.onRendered(function () {
});

Template.Upload.onDestroyed(function () {
});

function updateFile(file) {
    let reader = new FileReader();;
    reader.onload = function(fileLoadEvent) {
        Meteor.call('file-upload', file.name, reader.result);
    };
    reader.readAsBinaryString(file);

}