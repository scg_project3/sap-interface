/*****************************************************************************/
/* Authorization: Event Handlers */
/*****************************************************************************/
/* global Template, Router */

Template.Authorization.events({
    "change #company_id": function(e) {
        var company_id = $("#company_id").val();
        Session.set("company_id", parseInt(company_id));
    },
    "change #branch_id": function(e) {
        var branch_id = $("#branch_id").val();
        Session.set("branch_id", parseInt(branch_id));
    },
    "change #brand_id": function(e) {
        var brand_id = $("#brand_id").val();
        Session.set("brand_id", parseInt(brand_id));
    },
    'click #btnSaveAuth': function() {
        var inputStatus = inputCHKEmpty('auth-form');
        if (inputStatus) {
            var company_id = $("#company_id").val();
            var branch_id = $("#branch_id").val();
            var brand_id = $("#brand_id").val();
            Meteor.call('setDefaultAuth', company_id, branch_id, brand_id, (Session.get('session_user_profile')).user_id, function(error, result) {
                if (result) {
                    // alert('profile setup done !');
                    Router.go("/");
                } else {
                    alert('profile setup fail !');
                }
            });
        } else {
            // alert('profile setup fail !');
            return false;
        }
    }
});

/*****************************************************************************/
/* Authorization: Helpers */
/*****************************************************************************/
Template.Authorization.helpers({});

/*****************************************************************************/
/* Authorization: Lifecycle Hooks */
/*****************************************************************************/
Template.Authorization.onCreated(function() {
    this.autorun(function() {
        Meteor.subscribe('admin_uthorizationmainmenumap');
        Meteor.subscribe('admin_uthorizationsubmenumap');
        Meteor.subscribe('admin_userprofile');
        Meteor.subscribe('admin_usergroup');
        Meteor.subscribe('admin_usergroupmap');
        Meteor.subscribe('admin_authorizationprofile');
        Meteor.subscribe('admin_authorizationprofilemap');
        Meteor.subscribe('admin_authorizationbrandmap');
        Meteor.subscribe('admin_authorizationcompanymap');
        Meteor.subscribe('admin_authorizationdepartmentmap');
        Meteor.subscribe('conf_branchmap');
        Meteor.subscribe('conf_company');
        Meteor.subscribe('conf_branch');
        Meteor.subscribe('conf_brand');
        Meteor.subscribe('conf_businessunit');
    });
});

Template.Authorization.onRendered(function() {});

Template.Authorization.onDestroyed(function() {});
