/*****************************************************************************/
/* Login: Event Handlers */
/*****************************************************************************/
Template.Login.events({
    'submit form': function(event) {
        event.preventDefault();
        var username = $('[name=username]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(username, password, function(error) {
            if (error) {
                alert(error.reason);
            } else {
                Router.go("/");
            }
        });
    }
});

/*****************************************************************************/
/* Login: Helpers */
/*****************************************************************************/
Template.Login.helpers({});

/*****************************************************************************/
/* Login: Lifecycle Hooks */
/*****************************************************************************/
Template.Login.onCreated(function() {
    $('.header-pos').css('display', 'none');
});

Template.Login.onRendered(function() {
    $('.header-pos').css('display', 'none');
});

Template.Login.onDestroyed(function() {
    $('.header-pos').css('display', 'block');
});
