/*****************************************************************************/
/* AuthorizationSetup: Event Handlers */
/*****************************************************************************/
/* global Template, Router */

Template.AuthorizationSetup.events({
    "change #company_id": function(e) {
        var company_id = $("#company_id").val();
        Session.set("company_id", parseInt(company_id));
    },
    "change #branch_id": function(e) {
        var branch_id = $("#branch_id").val();
        Session.set("branch_id", parseInt(branch_id));
    },
    "change #brand_id": function(e) {
        var brand_id = $("#brand_id").val();
        Session.set("brand_id", parseInt(brand_id));
    },
    'click #btnSaveAuth': function() {
        var company_id = $("#company_id").val();
        var branch_id = $("#branch_id").val();
        var brand_id = $("#brand_id").val();
        Meteor.call('setDefaultAuth', company_id, branch_id, brand_id, (Session.get('session_user_profile')).user_id, function(error, result) {
            if (result) {
                Modal.hide('AuthorizationSetup');
            } else {
                alert('profile setup fail !');
            }
        });
    }
});

/*****************************************************************************/
/* AuthorizationSetup: Helpers */
/*****************************************************************************/
Template.AuthorizationSetup.helpers({});

/*****************************************************************************/
/* AuthorizationSetup: Lifecycle Hooks */
/*****************************************************************************/
Template.AuthorizationSetup.onCreated(function() {
    this.autorun(function() {});
});

Template.AuthorizationSetup.onRendered(function() {});

Template.AuthorizationSetup.onDestroyed(function() {});
