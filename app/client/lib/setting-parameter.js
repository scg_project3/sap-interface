getCalcutatuontype = function(cal_type_id) {
    if (cal_type_id == 1) {
        var type_name = "Work Type"
    } else if (cal_type_id == 2) {
        var type_name = "Time Base"
    } else {
        var type_name = ""
    }
    return type_name
}

getIndicator = function(indicator_id) {
    if (indicator_id == 1) {
        var name = "In";
    } else if (indicator_id == 2) {
        var name = "Out";
    } else {
        var name = "";
    }
    return name
}

getTypeId = function(type_id) {
    if (type_id == 1) {
        var type_id_name = "บริการ"
    } else if (type_id == 2) {
        var type_id_name = "สินค้า"
    } else if (type_id == 3) {
        var type_id_name = "บริการ/สินค้า"
    } else {
        var type_id_name = ""
    }
    return type_id_name
}

getSelectStatus = function(status_id) {
    if (status_id == 1) {
        var name = "เช่า"
    } else if (status_id == 2) {
        var name = "สำรอง"
    } else {
        var name = ""
    }
    return name
}

getAssetStatus = function(asset_status) {
    if (asset_status == 1) {
        var asset_status = 'ใช้งานได้'
    } else if (asset_status == 2) {
        var asset_status = 'ชำรุด'
    } else if (asset_status == 3) {
        var asset_status = 'รอซ่อมแซม'
    } else if (asset_status == 4) {
        var asset_status = 'เสื่อมสภาพ'
    } else if (asset_status == 5) {
        var asset_status = 'ขาย/ตัดจำหน่าย'
    } else {
        var asset_status = ""
    }
    return asset_status
}

getSelectModule = function(module_id) {
    if (module_id == 1) {
        var name = "Quotation"
    } else if (module_id == 2) {
        var name = "Job Oder"
    } else if (module_id == 3) {
        var name = "Warehouse"
    } else if (module_id == 4) {
        var name = "Calculation Note"
    } else if (module_id == 5) {
        var name = "Invoice"
    } else {
        var name = ""
    }
    return name
}

//phatja
getMaintenanceOrder = function(maintenance_id) {
    if (maintenance_id == 1) {
        var name = "แจ้งเสียหาย"
    } else if (maintenance_id == 2) {
        var name = "ตรวจสอบทั่วไป"
    } else {
        var name = "อื่นๆ"
    }
    return name;
}


getlistAssetStatusMA = function() {


    var arr = ["Select", "ใช้งานได้", "ชำรุด", "รอซ่อมแซม", "เสื่อมสภาพ", "ขาย/ตัดจำหน่าย"];
    return arr;
}



getlistMaintType = function() {


    var arr = ["Select", "ซ่อมในประเทศ", "ซ่อมต่างประเทศ"];

    return arr;
}


getRateType = function(rate_type_id) {
    var array = [{ id: 1, name: "Day Rate" }, { id: 2, name: "Week-Week Rate" }, { id: 3, name: "Week-Day Rate" }]
    if (rate_type_id == undefined) {
        return array
    } else {
        return array[rate_type_id]
    }
}

getSelectModuleList = function(module_id) {
    var moDuleList = {
        0: "",
        1: "Quotation",
        2: "Job Oder",
        3: "Calculation Note",
        4: "Invoice",
        5: "Tax Invoice",
        6: "Purchasing ",
        7: "MA",
        8: "SO"
    }
    return moDuleList[module_id]
}

leave_recording_type = function(val) {
    if (val == 1) {
        var type_name = "ลาป่วย";
    } else if (val == 2) {
        var type_name = "ลากิจ";
    } else if (val == 3) {
        var type_name = "ลาคลอด";
    } else if (val == 4) {
        var type_name = "ลาพักร้อน";
    } else {
        var type_name = "ลา";
    }
    return type_name;
}

getItemType = function(val) {
    if (val == 1) {
        var item_name = "Asset";
    } else if (val == 2) {
        var item_name = "Material";
    } else {
        var item_name = "invalid";
    }
    return item_name;
}

getStatus = function(val) {
    if (val == 1) {
        var item_name = "สมบูรณ์";
    } else if (val == 2) {
        var item_name = "ชำรุด"
    } else {
        var item_name = "";
    }
    return item_name
}
