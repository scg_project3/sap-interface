Handlebars.registerHelper('genCurrency', function(data) {
    if (data) {
        return Currency.find({ currency_id: data });
    } else {
        return Currency.find();
    }
});

Handlebars.registerHelper('genJobtype', function(data) {
    if (data) {
        return Jobtype.find({ "job_type_id": data, "job_type_active": true });
    } else {
        if ((Session.get('session_user_profile')).default_company_id && (Session.get('session_user_profile')).default_brand_id) {
            var company_id = (Session.get('session_user_profile')).default_company_id;
            var brand_id = (Session.get('session_user_profile')).default_brand_id;
            return Jobtype.find({ "company_id": company_id, "brand_id": brand_id, "job_type_active": true });
        }
    }
});

Handlebars.registerHelper('genPricetype', function(data) {
    if (data) {
        return Pricetype.find({ "price_type_id": data, "price_type_active": true });
    } else {
        if ((Session.get('session_user_profile')).default_company_id && (Session.get('session_user_profile')).default_brand_id) {
            var company_id = (Session.get('session_user_profile')).default_company_id;
            var brand_id = (Session.get('session_user_profile')).default_brand_id;
            return Pricetype.find({ "company_id": company_id, "brand_id": brand_id, "price_type_active": true });
        }
    }
});

Handlebars.registerHelper('genCustomer', function(data) {
    if (data) {

    } else {
        if ((Session.get('session_user_profile')).default_company_id) {
            var company_id = (Session.get('session_user_profile')).default_company_id;
            return Customer.find({ "company_id": company_id });
        }
    }
});

Handlebars.registerHelper('genCustomerService', function(data) {
    if (data) {} else {
        return Customerservice.find({}, { sort: { customer_service_id: 1 } }).map(function(data) {
            var CrewDb = Crew.findOne({ crew_id: data.crew_id });
            data.cs_name = CrewDb ? (CrewDb.crew_firstname_en + " " + CrewDb.crew_lastname_en) : "-";
            return data;
        });
    }
});

Handlebars.registerHelper('genMarketing', function(data) {
    if (data) {
        return Marketing.find({ "marketing_id": data, "marketing_active": true });
    } else {
        if ((Session.get('session_user_profile')).default_company_id && (Session.get('session_user_profile')).default_brand_id) {
            var company_id = (Session.get('session_user_profile')).default_company_id;
            var brand_id = (Session.get('session_user_profile')).default_brand_id;
            return Marketing.find({ "company_id": company_id, "brand_id": brand_id, "marketing_active": true });
        }
    }
});

Handlebars.registerHelper('genCompany', function(id) {
    return Company.find({ "company_id": id, "company_active": true }, { sort: { company_no: 1 } });
});

Handlebars.registerHelper('genBranch', function(id) {
    return Branch.find({ "branch_id": id, "branch_active": true }, { sort: { branch_no: 1 } });
});

Handlebars.registerHelper('genBrand', function(id) {
    return Brand.find({ "brand_id": id, "brand_active": true }, { sort: { brand_no: 1 } });
});

Handlebars.registerHelper('genBusinessunit', function(id) {
    return Businessunit.find({ "business_unit_id": id, "business_unit_active": true }, { sort: { business_unit_no: 1 } })
});

Handlebars.registerHelper('equals', function(a, b) {
    return a === b;
});

Handlebars.registerHelper('showShootingDate', function(data) {
    return arrayToDate(data);
});
