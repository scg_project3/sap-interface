Handlebars.registerHelper('listCompany', function() {
    // console.log("company_id", Session.get("company_id"))
    // console.log("branch_id", Session.get("branch_id"))
    // console.log("brand_id", Session.get("brand_id"))
    return Company.find({ "company_id": { $in: (Session.get('session_user_profile')).company_id }, "company_active": true }, { sort: { company_no: 1 } });
});

Handlebars.registerHelper('listBranch', function() {
    // console.log("company_id", Session.get("company_id"))
    // console.log("branch_id", Session.get("branch_id"))
    // console.log("brand_id", Session.get("brand_id"))
    if (Session.get("company_id")) {
        var company_id = parseInt(Session.get("company_id"));
        return Branch.find({ "company_id": company_id, "branch_active": true }, { sort: { branch_no: 1 } });
    }
});

Handlebars.registerHelper('listBrand', function() {
    // console.log("company_id", Session.get("company_id"))
    // console.log("branch_id", Session.get("branch_id"))
    // console.log("brand_id", Session.get("brand_id"))
    if (Session.get("company_id")) {
        var company_id = parseInt(Session.get("company_id"));
        return Brand.find({ "brand_id": { $in: (Session.get('session_user_profile')).brand_id }, "company_id": company_id, "brand_active": true }, { sort: { brand_no: 1 } });
    }
});

Handlebars.registerHelper('listBusinessunitd', function() {
    if ((Session.get('session_user_profile')).default_brand_id) {
        var branch_id = parseInt((Session.get('session_user_profile')).default_brand_id);
        var BranchmapDb = Branchmap.find({ "branch_id": branch_id }).fetch();
        var arrCompany = [];
        _.each(BranchmapDb, function(value, key, list) {
            arrCompany.push(value.company_id);
        });
        return Businessunit.find({ "branch_id": { $in: arrCompany } }, { sort: { business_unit_no: 1 } })
    }
});

Handlebars.registerHelper('defaultCompanyName', function() {
    if ((Session.get('session_user_profile')).default_company_id) {
        return (Company.findOne({ company_id: (Session.get('session_user_profile')).default_company_id })).company_shortname;
    }
});

Handlebars.registerHelper('defaultBranchName', function() {
    if ((Session.get('session_user_profile')).default_branch_id) {
        return (Branch.findOne({ branch_id: (Session.get('session_user_profile')).default_branch_id })).branch_shortname;
    }
});

Handlebars.registerHelper('defaultBrandName', function() {
    if ((Session.get('session_user_profile')).default_brand_id) {
        return (Brand.findOne({ brand_id: (Session.get('session_user_profile')).default_brand_id })).brand_name;
    }
});
