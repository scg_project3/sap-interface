arrayToDate = function(arrayDate) {
    var dateReturn = "";
    //console.log(arrayDate)
    _.each(arrayDate, function(v, k, l) {
        var start_date = moment(v.start_date).format('DD/MM/YYYY')
        var end_date = moment(v.end_date).format('DD/MM/YYYY')
        if (start_date == end_date) {
            dateReturn += start_date + ","
        } else {
            dateReturn += start_date + "-" + end_date + ","
        }
    });
    dateReturn = dateReturn.substring(0, dateReturn.length - 1);
    return dateReturn;
}

dateToArray = function(date) {
    var dateRetrun = [];
    var dateArray = date.split(",");
    _.each(dateArray, function(v, k, l) {
        var dateSplit = v.split("-")
        if (dateSplit.length > 1) {
            var dateOje = {
                start_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString(),
                end_date: moment(dateSplit[1], "DD/MM/YYYY H:mm").toISOString()
            };
        } else {
            var dateOje = {
                start_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString(),
                end_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString()
            };
        }
        dateRetrun.push(dateOje)
    });
    return dateRetrun;
}


listDateToFormatITBC = function(StrAllDate) {
    var sumDate = "";
    var date = "";
    var sym = "";
    var state = 0;
    var DateBuffer = StrAllDate;
    for (var i = 0; i < DateBuffer.length; i++) {
        //alert(i)
        var start = moment(DateBuffer[i], "DD/MM/YYYY");
        var end = moment(DateBuffer[i + 1], "DD/MM/YYYY");
        var range = moment.range(start, end);
        if (range.diff('days') == 1) {
            if (state == 0) {
                date = moment(DateBuffer[i]).format("DD/MM/YYYY")
                state = 1
                sym = "-";
            } else {
                sym = "-";
            }

        } else if (range.diff('days') > 1) {
            if (state == 1) {
                sumDate += date + sym + moment(DateBuffer[i]).format("DD/MM/YYYY") + ",";
                date = "";
                sym = "";
                state = 0
            } else {
                sumDate += moment(DateBuffer[i]).format("DD/MM/YYYY") + ",";
                state = 0
            }

        } else {
            if (state == 1) {
                sym = "-";
                sumDate += date + sym + moment(DateBuffer[i]).format("DD/MM/YYYY");
                date = "";
                sym = "";
                state = 0
            } else {
                sumDate += moment(DateBuffer[i]).format("DD/MM/YYYY");
                date = "";
                sym = "";
                state = 0
            }

        }

    }
    return sumDate;
}

FormatITBCTolistDate = function(data) {
    var dateReturn = [];
    _.each(data, function(v, k, l) {
        var start = new Date(v.start_date);
        var end = new Date(v.end_date);
        var dr = moment.range(start, end);
        _.each(dr.toArray('days'), function(v2, k2, l2) {
            dateReturn.push(v2);
        });
    });
    return dateReturn;
}

checkDateToArray = function(data) {
    var validateDate = true;
    _.each(data, function(v, k, l) {
        var start_date = moment(v.start_date).isValid();
        var end_date = moment(v.end_date).isValid();
        if (validateDate == true) {
            validateDate = (start_date == true ? true : false)
            validateDate = (end_date == true ? true : false)
        }
        if (validateDate == true) {
            validateDate = (start_date == true ? true : false)
        }
    });
    return validateDate;
}



///////////////////////////////////////////////////////// How To Use/////////////////////////////////////////////////////////////////////////////

// job_dateselect = [
//     new Date("2017-01-14T17:00:00.0Z"),
//     new Date("2017-01-15T17:00:00.0Z"),
//     new Date("2017-01-17T17:00:00.0Z"),
//     new Date("2017-01-20T17:00:00.0Z"),
//     new Date("2017-01-21T17:00:00.0Z"),
//     new Date("2017-01-25T17:00:00.0Z"),
//     new Date("2017-01-26T17:00:00.0Z"),
//     new Date("2017-01-30T17:00:00.0Z"),
//     new Date("2017-01-31T17:00:00.0Z"),
// ]
// console.log("job_dateselect", job_dateselect)
// var showDate = listDateToFormatITBC(job_dateselect) // เอาจาก job_dateselect มาแสดง
// console.log("listDateToFormatITBC", showDate)

// var arrDate = dateToArray(showDate); //แปลงจาก FormatITBC ไปเป็น Array Date เพื่อ Save
// console.log("arrDate", arrDate)

// var saveDate = FormatITBCTolistDate(arrDate) // จาก Array Date ไปเป็น list Date ของ job_dateselect
// console.log("saveDate", saveDate)









///////////////////////////////////////////////////////// Note Use/////////////////////////////////////////////////////////////////////////////










ConvertShoottingDate = function(RangDate) {
    var MountJS = { 1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7, 9: 8, 10: 9, 11: 10, 12: 11 };
    var sumDate = "";
    var DateBuffer = "";
    _.each(RangDate.split(','), function(v1, k1, l1) {
        _.each(v1.split('-'), function(v2, k2, l2) {
            var v2ExtracDate = v2.split('/');
            var CurrentDate = moment(new Date(parseInt(v2ExtracDate[2]), MountJS[parseInt(v2ExtracDate[1])], parseInt(v2ExtracDate[0]), 0, 0, 0));
            if ((DateBuffer != "") && (k2 > 0)) {
                var diffDate = CurrentDate.diff(DateBuffer, 'days');
                if (diffDate > 2) {
                    var incrementDate = DateBuffer;
                    for (var i = 1; i < diffDate; i++) {
                        incrementDate = incrementDate.add("days", 1);
                        sumDate += incrementDate.format('DD/MM/YYYY') + ",";
                    }
                }
            }
            DateBuffer = moment(new Date(parseInt(v2ExtracDate[2]), MountJS[parseInt(v2ExtracDate[1])], parseInt(v2ExtracDate[0]), 0, 0, 0));
            sumDate += DateBuffer.format('DD/MM/YYYY') + ",";
        });
    });
    // console.log(sumDate);
    sumDate = sumDate.substring(0, sumDate.length - 1);
    return sumDate;
}
DeConvertShoottingDate = function(StrAllDate) {
    //console.log(StrAllDate);
    var sumDate = "";
    var date = "";
    var sym = "";
    var state = 0;
    var DateBuffer = StrAllDate;
    console.log(DateBuffer)
    for (var i = 0; i < DateBuffer.length; i++) {
        //alert(i)
        var start = moment(DateBuffer[i], "DD/MM/YYYY");
        var end = moment(DateBuffer[i + 1], "DD/MM/YYYY");
        var range = moment.range(start, end);
        if (range.diff('days') == 1) {
            if (state == 0) {
                date = DateBuffer[i]
                state = 1
                sym = "-";
            } else {
                sym = "-";
            }

        } else if (range.diff('days') > 1) {
            if (state == 1) {
                sumDate += date + sym + DateBuffer[i] + ",";
                date = "";
                sym = "";
                state = 0
            } else {
                sumDate += DateBuffer[i] + ",";
                state = 0
            }

        } else {
            if (state == 1) {
                sym = "-";
                sumDate += date + sym + DateBuffer[i];
                date = "";
                sym = "";
                state = 0
            } else {
                sumDate += DateBuffer[i];
                date = "";
                sym = "";
                state = 0
            }

        }

    }
    console.log(sumDate);
    return sumDate;
}
