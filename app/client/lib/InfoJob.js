getInfoJobordersub = function(job_sub_id) {
    var data = {}

    var JobordersubDb = Jobordersub.findOne({ job_sub_id: parseInt(job_sub_id) });

    var job_entry_id = JobordersubDb ? JobordersubDb.job_entry_id : 0;

    var JoborderentryDb = Joborderentry.findOne({ job_entry_id: job_entry_id });
    var quo_sub_version_id = JoborderentryDb ? JoborderentryDb.quo_sub_version_id : 0;

    var QuotationsubversionDb = Quotationsubversion.findOne({ quo_sub_version_id: quo_sub_version_id });
    var quo_sub_id = QuotationsubversionDb ? QuotationsubversionDb.quo_sub_id : 0;

    var QuotationsubDB = Quotationsub.findOne({ quo_sub_id: quo_sub_id });
    var quo_entry_id = QuotationsubDB ? QuotationsubDB.quo_entry_id : 0;

    var QuotationentryDb = Quotationentry.findOne({ quo_entry_id: quo_entry_id });

    data.Quotationentry = QuotationentryDb;
    data.Jobordersub = JobordersubDb;
    data.Quotationsub = QuotationsubDB;
    data.Quotationsubversion = QuotationsubversionDb;
    data.Joborderentry = JoborderentryDb;
    return data
}
