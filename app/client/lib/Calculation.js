calCust_que = function(quo_entry_id, quo_sub_id, quo_sub_version_id, crew_skill_id) {
    var QuotationcrewtimesheetDb = Quotationcrewtimesheet.find({ quo_entry_id: quo_entry_id, quo_sub_id: quo_sub_id, quo_sub_version_id: quo_sub_version_id, crew_skill_id: crew_skill_id }).fetch();
    var cust_que = 0;
    _.each(QuotationcrewtimesheetDb, function(v, k, l) {
        cust_que += v.quo_crew_timesheet_cust_que
    });
    return cust_que ? cust_que : 0
}


calHirerAmount = function(quo_entry_id, quo_sub_id, quo_sub_version_id, crew_skill_id, quo_crew_skill_qty, quo_crew_skill_day_rate) {

    var QuotationcrewtimesheetDb = Quotationcrewtimesheet.find({ quo_entry_id: quo_entry_id, quo_sub_id: quo_sub_id, quo_sub_version_id: quo_sub_version_id, crew_skill_id: crew_skill_id }).fetch();
    var hirer = 0;
    var cust_que = 0;

    _.each(QuotationcrewtimesheetDb, function(v, k, l) {

        cust_que += v.quo_crew_timesheet_cust_que
    });

    hirer = quo_crew_skill_qty * cust_que * quo_crew_skill_day_rate;

    return hirer ? hirer : 0;
}

calHirerAmountTotal = function(data) {
    var hirertotal = 0;
    _.each(data, function(v, k, l) {

        hirertotal += calHirerAmount(v.quo_entry_id, v.quo_sub_id, v.quo_sub_version_id, v.crew_skill_id, v.quo_crew_skill_qty, v.quo_crew_skill_day_rate);
    });
    return hirertotal ? hirertotal : 0;
}


calCust_ot = function(quo_entry_id, quo_sub_id, quo_sub_version_id, crew_skill_id) {
    var QuotationcrewtimesheetDb = Quotationcrewtimesheet.find({ quo_entry_id: quo_entry_id, quo_sub_id: quo_sub_id, quo_sub_version_id: quo_sub_version_id, crew_skill_id: crew_skill_id }).fetch();
    var cust_ot = 0;
    _.each(QuotationcrewtimesheetDb, function(v, k, l) {
        cust_ot += v.quo_crew_timesheet_crew_ot
    });
    return cust_ot ? cust_ot : 0;
}

calTotAmount = function(quo_entry_id, quo_sub_id, quo_sub_version_id, crew_skill_id, quo_crew_skill_qty, quo_crew_skill_ot_rate) {

    var QuotationcrewtimesheetDb = Quotationcrewtimesheet.find({ quo_entry_id: quo_entry_id, quo_sub_id: quo_sub_id, quo_sub_version_id: quo_sub_version_id, crew_skill_id: crew_skill_id }).fetch();

    var tot = 0;
    var cust_ot = 0;

    _.each(QuotationcrewtimesheetDb, function(v, k, l) {
        cust_ot += v.quo_crew_timesheet_crew_ot
    });

    tot = quo_crew_skill_qty * cust_ot * quo_crew_skill_ot_rate;

    return tot ? tot : 0;
}

calTotAmountTotal = function(data) {

    var tot_total = 0;

    _.each(data, function(v, k, l) {

        tot_total += calTotAmount(v.quo_entry_id, v.quo_sub_id, v.quo_sub_version_id, v.crew_skill_id, v.quo_crew_skill_qty, v.quo_crew_skill_ot_rate);
    });

    return tot_total ? tot_total : 0;
}

calCrewQueByList = function(db) {

    var cust_que = 0;
    _.each(db, function(v, k, l) {
        var QuotationcrewtimesheetDb = Quotationcrewtimesheet.find({ quo_entry_id: v.quo_entry_id, quo_sub_id: v.quo_sub_id, quo_sub_version_id: v.quo_sub_version_id, crew_skill_id: v.crew_skill_id }).fetch();
        _.each(QuotationcrewtimesheetDb, function(v, k, l) {
            cust_que += v.quo_crew_timesheet_cust_que
        });
    });
    return cust_que ? cust_que : 0
}

calproductQuo = function(db) {
    var queSum = 0
    _.each(db, function(v, k, l) {
        queSum += calProductQueByList(v.quo_product_id)
        console.log("queSum", queSum)
    });

    return queSum
}

calProductQueByList = function(quo_product_id) {
    var calQue = 0;
    var db = Quotationproducttimesheet.find({ quo_product_id: parseInt(quo_product_id) }).fetch();
    _.each(db, function(v, k, l) {
        var dBw = Worktypeproduct.findOne({ work_type_product_id: v.work_type_product_id });
        queNumber = dBw ? parseFloat(dBw.work_type_product_que) : 0
        calQue = calQue + queNumber
    });
    return calQue
}

calProductNetAmount = function(db) {
    var calPrice = 0
    _.each(db, function(v, k, l) {
        calPrice += parseFloat(v.quo_product_quantity) * calProductQueByList(v.quo_product_id) * parseFloat(v.quo_product_price)
    });
    return calPrice
}

calProductDiscount = function(db) {
    var calPrice = 0
    _.each(db, function(v, k, l) {
        calPrice += (parseFloat(v.quo_product_quantity) * (parseFloat(v.quo_product_price) * calProductQueByList(v.quo_product_id)) * v.quo_product_discount_percent) / 100
    });
    return calPrice
}


///////////////////////////////////////////////////////////////////// Job Order

calJobCust_que = function(job_entry_id, job_sub_id, crew_skill_id) {
    var JobordercrewskilltimesheetDb = Jobordercrewskilltimesheet.find({ job_entry_id: job_entry_id, job_sub_id: job_sub_id, crew_skill_id: crew_skill_id }).fetch();
    var cust_que = 0;
    _.each(JobordercrewskilltimesheetDb, function(v, k, l) {
        cust_que += v.job_crew_skill_timesheet_cust_que
    });
    return cust_que ? cust_que : 0
}


calJobHirerAmount = function(job_entry_id, job_sub_id, crew_skill_id, job_crew_skill_qty, job_crew_skill_day_rate) {

    var JobordercrewskilltimesheetDb = Jobordercrewskilltimesheet.find({ job_entry_id: job_entry_id, job_sub_id: job_sub_id, crew_skill_id: crew_skill_id }).fetch();
    var hirer = 0;
    var cust_que = 0;
    _.each(JobordercrewskilltimesheetDb, function(v, k, l) {

        cust_que += v.job_crew_skill_timesheet_cust_que
    });

    hirer = job_crew_skill_qty * cust_que * job_crew_skill_day_rate;

    return hirer ? hirer : 0;
}

calJobHirerAmountTotal = function(data) {
    var hirertotal = 0;
    _.each(data, function(v, k, l) {

        hirertotal += calHirerAmount(v.job_entry_id, v.job_sub_id, v.job_sub_version_id, v.crew_skill_id, v.job_crew_skill_qty, v.job_crew_skill_day_rate);
    });
    return hirertotal ? hirertotal : 0;
}


calJobCust_ot = function(job_entry_id, job_sub_id, crew_skill_id) {
    var JobordercrewskilltimesheetDb = Jobordercrewskilltimesheet.find({ job_entry_id: job_entry_id, job_sub_id: job_sub_id, crew_skill_id: crew_skill_id }).fetch();
    var cust_ot = 0;
    _.each(JobordercrewskilltimesheetDb, function(v, k, l) {
        cust_ot += v.job_crew_skill_timesheet_crew_ot
    });
    return cust_ot ? cust_ot : 0;
}

calJobTotAmount = function(job_entry_id, job_sub_id, crew_skill_id, job_crew_skill_qty, job_crew_skill_ot_rate) {

    var JobordercrewskilltimesheetDb = Jobordercrewskilltimesheet.find({ job_entry_id: job_entry_id, job_sub_id: job_sub_id, crew_skill_id: crew_skill_id }).fetch();

    var tot = 0;
    var cust_ot = 0;

    _.each(JobordercrewskilltimesheetDb, function(v, k, l) {
        cust_ot += v.job_crew_skill_timesheet_crew_ot
    });

    tot = job_crew_skill_qty * cust_ot * job_crew_skill_ot_rate;

    return tot ? tot : 0;
}

calJobTotAmountTotal = function(data) {

    var tot_total = 0;

    _.each(data, function(v, k, l) {

        tot_total += calTotAmount(v.job_entry_id, v.job_sub_id, v.job_sub_version_id, v.crew_skill_id, v.job_crew_skill_qty, v.job_crew_skill_ot_rate);
    });

    return tot_total ? tot_total : 0;
}

calJobCrewQueByList = function(db) {

    var cust_que = 0;
    _.each(db, function(v, k, l) {
        var JobordercrewskilltimesheetDb = Jobordercrewskilltimesheet.find({ job_entry_id: v.job_entry_id, job_sub_id: v.job_sub_id, crew_skill_id: v.crew_skill_id }).fetch();
        _.each(JobordercrewskilltimesheetDb, function(v, k, l) {
            cust_que += v.job_crew_skill_timesheet_cust_que
        });
    });
    return cust_que ? cust_que : 0
}
