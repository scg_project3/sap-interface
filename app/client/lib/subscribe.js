checksubready = function(subArray){
	var status = true
	_.each(subArray, function(v, k, l){
		if (!v.ready()) {
			status = false
		}
	});
	return status
}
