reqUserProfile = function() {
    var data = {};
    var user_id = reqUser_id();
    var arrCompany = [];
    var arrBrand = [];
    data.user_id = user_id;
    var UserprofileDb = Userprofile.findOne({ user_id: user_id });
    data.default_company_id = UserprofileDb ? UserprofileDb.default_company_id : null;
    data.default_branch_id = UserprofileDb ? UserprofileDb.default_branch_id : null;
    data.default_brand_id = UserprofileDb ? UserprofileDb.default_brand_id : null;
    var user_profile_id = UserprofileDb ? UserprofileDb.user_profile_id : 0;
    var usergroupmapDb = Usergroupmap.findOne({ user_profile_id: user_profile_id });
    var user_group_id = usergroupmapDb ? usergroupmapDb.user_group_id : 0;
    var AuthorizationprofilemapDb = Authorizationprofilemap.findOne({ user_group_id: user_group_id });
    var authorization_profile_id = AuthorizationprofilemapDb ? AuthorizationprofilemapDb.authorization_profile_id : 0;
    var AuthorizationcompanymapDb = Authorizationcompanymap.find({ authorization_profile_id: authorization_profile_id }).fetch();
    _.each(AuthorizationcompanymapDb, function(value, key, list) {
        arrCompany.push(value.company_id);
    });
    var AuthorizationbrandDb = Authorizationbrandmap.find({ authorization_profile_id: authorization_profile_id }).fetch();
    _.each(AuthorizationbrandDb, function(value, key, list) {
        arrBrand.push(value.brand_id);
    });
    data.company_id = arrCompany;
    data.brand_id = arrBrand;
    return data;
}

reqUser_id = function() {
    return Meteor.user() ? Meteor.user().profile.user_id : 0;
}

reqGroup = function() {
    var user_id = reqUser_id();
    var UserprofileDb = Userprofile.findOne({ user_id: user_id });
    var user_profile_id = UserprofileDb ? UserprofileDb.user_profile_id : 0

    var usergroupmapDb = Usergroupmap.findOne({ user_profile_id: user_profile_id });
    var user_group_id = usergroupmapDb ? usergroupmapDb.user_group_id : 0
        // Examp
    return user_group_id;
    // usergroup.find({user_id:user_id});
}

reqAuthorizationProfile = function() {
    var user_id = reqUser_id();
    var UserprofileDb = Userprofile.findOne({ user_id: user_id });
    var user_profile_id = UserprofileDb ? UserprofileDb.user_profile_id : 0
    var UsergroupmapDb = Usergroupmap.findOne({ user_profile_id: user_profile_id });
    var user_group_id = UsergroupmapDb ? UsergroupmapDb.user_group_id : 0
    var AuthorizationprofilemapDb = Authorizationprofilemap.findOne({ user_group_id: user_group_id });
    var authorization_profile_id = AuthorizationprofilemapDb ? AuthorizationprofilemapDb.authorization_profile_id : 0
    return authorization_profile_id;
}

// PhatJA

reqFilterIDComp = function() {
    var auth_prof_id = reqAuthorizationProfile();
    var a = Authorizationcompanymap.find({ authorization_profile_id: auth_prof_id }).fetch();
    var arr = [];
    _.each(a, function(v, k) {
        arr.push(v.company_id);
    });
    return arr;
}

reqFilterIDBrand = function(company_id) {
    var auth_prof_id = reqAuthorizationProfile();
    var a = Authorizationbrandmap.find({ authorization_profile_id: auth_prof_id, company_id: company_id }).fetch();
    var arr = [];
    _.each(a, function(v, k) {
        arr.push(v.brand_id);
    });
    return arr;
}

reqFilterIDDept = function(company_id, brand_id) {
    var auth_prof_id = reqAuthorizationProfile();
    var a = Authorizationdepartmentmap.find({ authorization_profile_id: auth_prof_id, brand_id: brand_id, company_id: company_id }).fetch();
    var arr = [];
    _.each(a, function(v, k) {
        arr.push(v.department_id);
    });
    return arr;
}

reqFilterDataComp = function() {
    var arr = reqFilterIDComp();
    var r = Company.find({ company_id: { $in: arr }, company_active: true }, { sort: { company_no: 1 } }).fetch();
    return r;
}

reqFilterDataBrand = function(company_id) {
    var arr = reqFilterIDBrand(company_id);
    var r = Brand.find({ brand_id: { $in: arr }, brand_active: true }, { sort: { brand_no: 1 } }).fetch();
    return r;
}

reqFilterDataDept = function(company_id, brand_id) {
    var arr = reqFilterIDDept(company_id, brand_id);
    var r = Department.find({ department_id: { $in: arr }, department_active: true }, { sort: { department_no: 1 } }).fetch();
    return arr;
}

mongoJoin = function(listcollect, collectionfind, pri_key) {
    _.each(listcollect, function(v, k) {
        listcollect[k]["_id_old"] = v._id;
        _.each(collectionfind, function(v2, k2) {
            var f = {};
            f[pri_key[k2]] = listcollect[k][pri_key[k2]];
            var r = collectionfind[k2].findOne(f);
            listcollect[k] = jQuery.extend(listcollect[k], r);

        });

        listcollect[k]["_id"] = listcollect[k]["_id_old"];
        delete listcollect[k]["_id_old"];
    });
    return listcollect;
}

reqDetailUser = function() {
    var user_id = reqUser_id();
    var userg = Usergroupmap.find({ user_profile_id: user_id }).fetch();
    var r = mongoJoin(
        userg, [Usergroup, Userprofile, Authorizationprofilemap, Authorizationprofile], ["user_group_id", "user_profile_id", "user_group_id", "authorization_profile_id"]
    );
    return r;
}

/*


filterProdDept = function(company_id,brand_id){
    var f = reqFilterIDDept(company_id,brand_id);
    var p = Product.find({ department_id:{$in:f} }).fetch();
    return p;
}

filterProdDeptFlag = function(company_id,brand_id){
    var f = reqFilterIDDept(company_id,brand_id);
    var p = Product.find().fetch();
    _.each(p,function(v,k){
            var c = _.indexOf(f, v.department_id);
            p[k].state_input = c < 0 ? "disabled" : "";     
    });
    return p;
}
*/
