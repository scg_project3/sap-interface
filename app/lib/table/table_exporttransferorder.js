import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.ExportTransferOrder = new Tabular.Table({
    name: "ExportTransferOrder",
    collection: ArchiveOrder,
    columns: [
        {
            data: "lpn_no",
            title: "LPN No",
            width: "25%"
        },
        {
            data: "transfer_order_no",
            title: "Transfer Order No",
            render: function (val, type, doc) {
                return val;
            },
            width: "30%"
        },
        {
            data: "movement_type",
            title: "Movement Type",
            width: "5%"
        },
        {
            data: "archive_date", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
            width: "35"
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {
                return "<button href='#' data-title='" + doc.lpn_no + "' data-id='" + val + "' class='btnViewExportTransferOrder btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";
            },
            width: "5%"
        }
    ],
    order: [[3, "desc"]],
    pageLength: 5
});