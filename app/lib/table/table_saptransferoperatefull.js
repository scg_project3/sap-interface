
import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.SapTransferOperateFull = new Tabular.Table({
    name: "SapTransferOperateFull",
    collection: SapTransferOperate,
    columns: [
        {
            data: "transfer_order_number",
            title: "Transfer Order Number",
            render: function (val, type, doc) {
                return val;
            }
        },
        {
            data: "transfer_order_item",
            title: "Transfer Order Item"
        },
        {
            data: "movement_type",
            title: "Movement Type"
        },
        {
            data: "material_number",
            title: "Material Number",
        },
        {
            data: "batch_no",
            title: "Batch No",
        },
        {
            data: "sales_and_distribution_document_number",
            title: "Sales And Distribution Document Number"
        },
        {
            data: "weight",
            title: "Weight",
        },
        {
            data: "destination_storage_unit_number",
            title: "Destination Storage Unit Number"
        },
        {
            data: "ship_to",
            title: "Ship To"
        },
        {
            data: "sync",
            title: "Sync",
            render: function (val, type, doc) {
                if (val === 0) {
                    return "<span class=\"badge badge-warning\">Un Sync</span>";
                } else {
                    return "<span class=\"badge badge-success\">Synced</span>";
                }
            }
        },
        {
            data: "update_time", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
            width: "20%"
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {
                let action = "";
                if (doc.sync === 0) {
                    action += "<button data-title='" + (doc.transfer_order_number + "-" + doc.transfer_order_item) + "' data-id='" + val + "' class='btnSyncSapTransferOperate btn btn-sm btn-success' ><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></button>";
                }
                action += "<button href='#' data-title='" + (doc.transfer_order_number + "-" + doc.transfer_order_item) + "' data-id='" + val + "' class='btnViewSapTransferOperate btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";

                return action;
            },
        }
    ],
    order: [[9, "asc"], [10, "desc"]],
    pageLength: 20
});