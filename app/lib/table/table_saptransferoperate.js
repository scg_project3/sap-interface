import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.SapTransferOperate = new Tabular.Table({
    name: "SapTransferOperate",
    collection: SapTransferOperate,
    columns: [
        {
            data: "transfer_order_number",
            title: "Transfer Order Number",
            render: function (val, type, doc) {
                return val;
            },
            width: "30%"
        },
        {
            data: "transfer_order_item",
            title: "Transfer Order Item",
            width: "5%"
        },
        {
            data: "material_number",
            title: "Material Number",
            width: "30%"
        },
        {
            data: "weight",
            title: "Weight",
            width: "5%"
        },
        {
            data: "update_time", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
            width: "20%"
        },
        {
            data: "sync"
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {

                let action = "";
                if (doc.sync === 0) {
                    action += "<button data-title='" + (doc.transfer_order_number + "-" + doc.transfer_order_item) + "' data-id='" + val + "' class='btnSyncSapTransferOperate btn btn-sm btn-success' ><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></button>";
                }
                action += "<button href='#' data-title='" + (doc.transfer_order_number + "-" + doc.transfer_order_item) + "' data-id='" + val + "' class='btnViewSapTransferOperate btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";

                return action;
            },
            width: "5%"
        }
    ],
    order: [[4, "desc"], [5, "asc"]],
    columnDefs: [
        {
            targets: [ 5 ],
            visible: false
        }
    ],
    pageLength: 5
});