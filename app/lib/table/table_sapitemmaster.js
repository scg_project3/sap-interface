import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.SapItemMaster = new Tabular.Table({
    name: "SapItemMaster",
    collection: SapItemMaster,
    columns: [
        {
            data: "material_number",
            title: "Material Number",
            render: function (val, type, doc) {
                return val;
            },
            width: "40%"
        },
        {
            data: "material_group",
            title: "Material Group",
            width: "5%"
        },
        {
            data: "weight_per_bag",
            title: "Weight Per Bag",
            width: "5%"
        },
        {
            data: "base_unit_of_measure",
            title: "Base Unit",
            width: "5%"
        },
        {
            data: "update_time", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
            width: "40%"
        },
        {
            data: "sync",
            title: "Sync"
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {
                let action = "";
                if (doc.sync === 0) {
                    action += "<button data-title='" + doc.material_number + "' data-id='" + val + "' class='btnSyncSapItemMaster btn btn-sm btn-success' ><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></button>";
                }
                action += "<button data-title='" + doc.material_number + "' data-id='" + val + "' class='btnViewSapItemMaster btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";
                return action;
            },
            width: "5%"
        }
    ],
    order: [[4, "desc"], [5, "asc"]],
    columnDefs: [
        {
            targets: [ 5 ],
            visible: false
        }
    ],
    pageLength: 5
});