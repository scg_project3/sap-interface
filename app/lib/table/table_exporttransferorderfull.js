import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.ExportTransferOrderFull = new Tabular.Table({
    name: "ExportTransferOrderFull",
    collection: ArchiveOrder,
    columns: [
        {
            data: "lpn_no",
            title: "LPN No./SU",
        },
        {
            data: "transfer_order_no",
            title: "Transfer Order No",
            render: function (val, type, doc) {
                return val;
            },
        },
        {
            data: "movement_type",
            title: "Movement Type",
        },
        {
            data: "transfer_order_item",
            title: "Transfer Order Item"
        },
        {
            data: "item_code",
            title: "Item Code",
        },
        {
            data: "lot_no",
            title: "Lot No",
        },
        {
            data: "delivery_order_no",
            title: "Delivery Order No",
        },
        {
            data: "weight",
            title: "Weight"
        },
        {
            data: "reject_reason",
            title: "Reject Reason",
        },
        {
            data: "actual_weights",
            title: "Actual Weight"
        },
        {
            data: "archive_date", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {
                return "<button href='#' data-title='" + doc.lpn_no + "' data-id='" + val + "' class='btnViewExportTransferOrder btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";
            },
        }
    ],
    order: [[10, "desc"]],
    pageLength: 20
});