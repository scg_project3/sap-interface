import Tabular from 'meteor/aldeed:tabular';
TabularTables = {};

TabularTables.Users = new Tabular.Table({
    name: "Users",
    collection: Meteor.users,
    columns: [
        { data: "username", title: "Username" }, {
            data: "profile",
            title: "Email",
            render: function(val, type, doc) {
                return val.email;
            }
        },
        { data: "profile.firstname", title: "Firstname" },
        { data: "profile.lastname", title: "Lastname" },
        { data: "profile.tel_number", title: "Tel" }, {
            data: "profile.role_id",
            title: "User role",
            render: function(val, type, doc) {
                var userroletDb = Roles.findOne({ role_id: val });
                var queryuserrole = userroletDb ? userroletDb.role_name : "";
                return queryuserrole;
            }
        }, {
            data: "profile.user_id",
            title: "Action",
            render: function(val, type, doc) {
                return "<button class='btn btn-sm btn-warning fa fa-pencil-square-o btneditfile' id='btnEditUser' user_id=" + val + "></button> <button class='btn btn-sm btn-danger fa fa-trash-o btnDeleteuser' user_id=" + val + "></button>"
            }
        },
    ]
});