import Tabular from 'meteor/aldeed:tabular';

TabularTables = {};

TabularTables.SapItemMasterFull = new Tabular.Table({
    name: "SapItemMasterFull",
    collection: SapItemMaster,
    columns: [
        {
            data: "material_number",
            title: "Material Number",
            render: function (val, type, doc) {
                return val;
            },
            width: "20%"
        },
        {
            data: "material_description",
            title: "Material Description",
            width: "30%"
        },
        {
            data: "material_group",
            title: "Material Group",
            width: "5%"
        },
        {
            data: "base_unit_of_measure",
            title: "Base Unit",
            width: "5%"
        },
        {
            data: "secondary_unit_of_measure",
            title: "Secondary Unit",
            width: "5%"
        },
        {
            data: "weight_per_bag",
            title: "Weight per bag",
            width: "5%"
        },
        {
            data: "storage_type",
            title: "Storage Type",
            width: "5%"
        },
        {
            data: "package_type",
            title: "Package Type",
            width: "5%"
        },
        {
            data: "deletion_flag",
            title: "Deletion Flag",
            width: "5%"
        },
        {
            data: "sync",
            title: "Sync",
            render: function (val, type, doc) {
                if (val === 0) {
                    return "<span class=\"badge badge-warning\">Un Sync</span>";
                } else {
                    return "<span class=\"badge badge-success\">Synced</span>";
                }
            }
        },
        {
            data: "update_time", title: "Time", render: function (val, type, doc) {
                let d = new Date(val);
                return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            },
            width: "10%"
        },
        {
            data: "_id", title: "Action", render(val, type, doc) {
                let action = "";
                if (doc.sync === 0) {
                    action += "<button data-title='" + doc.material_number + "' data-id='" + val + "' class='btnSyncSapItemMaster btn btn-sm btn-success' ><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></button>";
                }
                action += "<button data-title='" + doc.material_number + "' data-id='" + val + "' class='btnViewSapItemMaster btn btn-sm btn-info'><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></button>";
                return action;
            },
            width: "5%"
        }
    ],
    order: [[10, "desc"]],
    pageLength: 20
});