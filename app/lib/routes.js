Router.onBeforeAction(function (pause) {
    if (!Meteor.userId()) {
        console.log('not login');
        this.render('Login');
    } else {
        this.next();
    }
});

Router.route('/', {
    name: 'dashboard',
    where: 'client'
});

Router.route('/report/sapmasteritem', {
    name: 'masterItem',
    where: 'client'
});

Router.route('/report/saptransferoperate', {
    name: 'transferOperate',
    where: 'client'
});

Router.route('/report/exporttransferorder', {
    name: 'exportTransferOrder',
    where: 'client'
});

Router.route('/test/upload', {
    name: 'Upload',
    where: 'client'
});