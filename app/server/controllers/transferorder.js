Meteor.methods({
    'insertTransferorder': function (data) {
        data.itemmaster_id = autoInc('transferorder')
        Transferorder.insert(data);
        return true;
    },
    'updateTransferorder': function (query, data) {
        Transferorder.update(query, {$set: data});
        return true;
    },

    'deleteTransferorder': function (itemMasterId) {
        Transferorder.remove({itemmaster_id: parseInt(itemMasterId)});
        return true;
    },
});