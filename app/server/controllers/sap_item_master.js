Meteor.methods({
    'insertSAPItemMaster': function (data) {
        SapItemMaster.insert(data);
        return true;
    },
    'updateSAPItemMaster': function (id, data) {
        SapItemMaster.update({_id: id}, {$set: data});
        return true;
    },

    'deleteSAPItemMaster': function (id) {
        SapItemMaster.remove({_id: id});
        return true;
    },
});