Meteor.methods({

'insertBankConfig': function (data) {
  		data.bankconfig_id = autoInc('bankconfig')
    	Bankconfig.insert(data);
    	return true;
  },
  'updateBankConfig':function(bankconfig_id,data){
  		Bankconfig.update({bankconfig_id:parseInt(bankconfig_id)},{$set:data})
  		return true;
  },
  
  'deleteBankConfig':function(bankconfig_id){
  	Bankconfig.remove({bankconfig_id:parseInt(bankconfig_id)});
  	return true;
  },
});