/*****************************************************************************/
/*  Server Methods */
/*****************************************************************************/

Meteor.methods({
    'insertusers': function(datausers) {
        var user_id = autoInc("user");
        var password = datausers.password;
        datausers.user_id = user_id;
        datausers.status = 1
        delete datausers.password;
        delete datausers.password_confirm;
        var c = Accounts.createUser({
            username: datausers.username,
            password: password,
            email: datausers.email,
            profile: datausers,

        });

        return c;
    },
    'updateusers': function(datausers) {
        console.log('bb', datausers)
        delete datausers.password;
        delete datausers.password_confirm;
        return Meteor.users.update({ "profile.user_id": datausers.user_id }, { $set: { profile: datausers } });
    },
    'setPassword': function(data) {
        var a = Accounts.setPassword(data.user_id, data.password, {
            logout: false
        })
        return a;
    },
    insert_role: function(data) {
        data.role_id = autoInc("role");
        data.dependency = ["Meteor.users", "Roles"]; // Edit Me
        Roles.insert(data);
    },
    'update_role': function(data) {
        data.role_id = parseInt(data.role_id);
        return Roles.update({ role_id: data.role_id }, { $set: data });
    },
    'delete_role': function(data) {
        data.role_id = parseInt(data.role_id);
        var a = Roles.findOne({ role_id: data.role_id });

        return Roles.remove({ role_id: data.role_id });
    },
    'updatedeleteuser': function(datausers) {
        console.log(datausers)
        return Meteor.users.update({ "profile.user_id": datausers.user_id }, {
            $set: {
                "profile.status": 2
            }
        });
    },


});
