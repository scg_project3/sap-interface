import fs from 'fs';
import path from 'path'

Meteor.methods({
    'file-upload': (fileName, fileData) => {
        let base = path.resolve('.');
        let appBase = base.replace('/.meteor/local/build/programs/server', '');
        let incomePath = path.join(appBase, '/server/sap_data/income');
        fs.writeFile(incomePath + "/" + fileName, fileData);
    },

    'reset-data': () => {
        ItemMaster.remove({"flag": "SAP"});
        SapItemMaster.remove({});
        SapTransferOperate.remove({});
        SapRecentUpdate.remove({});
        Transferorder.remove({"flag": "SAP"});
        ArchiveOrder.update({"sap_exported": 1}, {"$set": {"sap_exported": 0}}, {multi: true});
        return true;
    }
});