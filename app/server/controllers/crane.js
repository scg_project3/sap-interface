Meteor.methods({
    'addcrane': function (data) {
        data.crane_id = autoInc('crane');
        data.create_at = new Date();
        data.last_update = new Date();
        return Crane.insert(data);
    },
    'editcrane': function (crane_id, data) {
        data.bank = parseInt(data.bank);
        data.bay = parseInt(data.bay);
        data.level = parseInt(data.level);
        data.last_update = new Date();
        return Crane.update({
            crane_id: parseInt(crane_id)
        }, {
            $set: data
        });
    },
    'deletecrane': function (crane_id) {
        return Crane.remove({
            crane_id: parseInt(crane_id)
        })
    }
});
