Meteor.methods({
    'Addstorage': function (data) {
        data.storagelayout_id = autoInc('storagelayout');
        data.bank = parseInt(data.bank);
        data.bay = parseInt(data.bay);
        data.level = parseInt(data.level);
        data.built_status = false;
        data.create_at = new Date();
        data.last_update = new Date();
        return Storagelayout.insert(data);
    },
    'Editstorage': function (storagelayout_id, data) {
        data.bank = parseInt(data.bank);
        data.bay = parseInt(data.bay);
        data.level = parseInt(data.level);
        data.last_update = new Date();
        return Storagelayout.update({
            storagelayout_id: parseInt(storagelayout_id)
        }, {
                $set: data
            });
    },
    'Deletestorage': function (storagelayout_id) {
        return Storagelayout.remove({
            storagelayout_id: parseInt(storagelayout_id)
        })
    },
    'Builtstorage': function (storagelayout_id) {
        let data_layout = Storagelayout.findOne({
            storagelayout_id: parseInt(storagelayout_id)
        });
        for (let x = 1; x <= data_layout.bank; x++) {
            for (let y = 1; y <= data_layout.bay; y++) {
                for (let z = 1; z <= data_layout.level; z++) {
                    let data = {
                        storagelayout_id: data_layout.storagelayout_id,
                        crane_id: 1,
                        bank: x,
                        bay: y,
                        level: z,
                        status: "empty",
                        zone_id: []
                    }
                    Storageoperation.insert(data);
                }
            }
        }
        return Storagelayout.update({
            storagelayout_id: parseInt(storagelayout_id)
        }, {
                $set: { built_status: true }
            });
    }
});