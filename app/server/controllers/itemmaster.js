import fs from 'fs';
import path from 'path'

Meteor.methods({
    'insertItemMaster': function (data) {
        data.itemmaster_id = autoInc('itemmaster')
        ItemMaster.insert(data);
        return true;
    },
    'updateItemMaster': function (material_number, data) {
        ItemMaster.update({item_code: material_number}, {$set: data});
        let text = '';
        text += material_number + "\t";
        text += data.item_name + "\t";
        text += "A\t";
        text += "";

        let currentTime = new Date();
        let fileName = "GSCMST111" + currentTime.getFullYear();
        fileName += ('0' + (currentTime.getMonth() + 1)).slice(-2);
        fileName += ('0' + (currentTime.getDate())).slice(-2);
        fileName += ('0' + (currentTime.getHours())).slice(-2);
        fileName += ('0' + (currentTime.getMinutes())).slice(-2);
        fileName += ('0' + (currentTime.getSeconds())).slice(-2);
        fileName += ".txt";

        let base = path.resolve('.');
        let appBase = base.replace('/.meteor/local/build/programs/server','');
        let outcomeDir = path.join(appBase, '/server/sap_data/outcome');

        fs.writeFile(outcomeDir + "/" + fileName, text, function (err) {
            if (!err) {
                console.log("The file was saved!");
            }
        });
        return true;
    },

    'deleteItemMaster': function (itemMasterId) {
        ItemMaster.remove({itemmaster_id: parseInt(itemMasterId)});
        return true;
    },
});