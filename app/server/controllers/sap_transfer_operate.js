Meteor.methods({
    'insertSAPTransferOperate': function (data) {
        SapTransferOperate.insert(data);
        return true;
    },
    'updateSAPTransferOperate': function (id, data) {
        SapTransferOperate.update({_id: id}, {$set: data});
        return true;
    },

    'deleteSAPTransferOperate': function (id) {
        SapTransferOperate.remove({_id: id});
        return true;
    },
});