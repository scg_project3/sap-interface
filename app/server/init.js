// var base = process.env.PWD

// UploadServer.init({
//     tmpDir: base + "/public/.#upload/",
//     uploadDir: base + "/public/.#upload/",
//     uploadUrl: '/upload',
//     getDirectory: function(fileInfo, formData) {
//         var quo_entry_id = formData.quotation_id
//         return "attachment/"+quo_entry_id+"/";
//     },
//     finished: function(fileInfo, formFields) {
//         var filePath = fileInfo.subDirectory+fileInfo.name
//         Attachment.insert({
//             quo_entry_id:parseInt(formFields.quo_id),
//             quo_sub_id:parseInt(formFields.quo_sub_id),
//             description:formFields.attachment_desc,
//             file_path: filePath,
//             file_name: fileInfo.name
//         });
//         return fileInfo.name;
//     },
// });


Meteor.startup(function() {
    UploadServer.init({
        tmpDir: process.env.PWD + "/public/.#upload",
        uploadDir: process.env.PWD + "/public/.#upload/",
        checkCreateDirectories: true,
        getDirectory: function(fileInfo, formData) {
            // create a sub-directory in the uploadDir based on the content type (e.g. 'images')
            console.log("fileInfo", fileInfo)
            console.log("formData", formData)
            return '/' + formData.contentType + '/';
        },
        finished: function(fileInfo, formFields) {
            console.log("finished", fileInfo)
                // perform a disk operation
        }
    });
});
