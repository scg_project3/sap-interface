import {
    JoinServer
} from 'meteor-publish-join'

Meteor.publish('roles', function() { //
    return Roles.find();
});

Meteor.publish('crane', function() { //
    return Crane.find();
});

Meteor.publish('sap_item_master', function() { //
    return SapItemMaster.find();
});

Meteor.publish('sap_transfer_operate', function () {
    return SapTransferOperate.find();
});

Meteor.publish('sap_recent_update', function () {
    return SapRecentUpdate.find();
});

Meteor.publish('archiveorder', function () {
    return ArchiveOrder.find();
});

Meteor.publish('itemmaster', function () {
    return ItemMaster.find();
});

Meteor.publish('transferorder', function () {
  return Transferorder.find();
});