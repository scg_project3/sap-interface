DocNumbering = function(module_id, company_id, branch_id) {
    var DocumentnumberingDb = Documentnumbering.findOne({ document_numbering_module_id: module_id, company_id: company_id, branch_id: branch_id });
    var document_numbering_prefixnumber = DocumentnumberingDb ? DocumentnumberingDb.document_numbering_prefixnumber : ""
    var document_numbering_prefixyear = DocumentnumberingDb ? DocumentnumberingDb.document_numbering_prefixyear : ""
    var document_numbering_running = DocumentnumberingDb ? DocumentnumberingDb.document_numbering_running : ""
    var document_numbering_yreset = DocumentnumberingDb ? DocumentnumberingDb.document_numbering_yreset : ""
    var document_numbering_id = DocumentnumberingDb ? DocumentnumberingDb.document_numbering_id : ""
    var document_numbering_running = DocumentnumberingDb ? parseInt(DocumentnumberingDb.document_numbering_running) : 0
    var prefixyear = moment().format(document_numbering_prefixyear)
    var strAutoInc = "";
    var docNo = "";
    var NoConfig = ["", "QU", "JO", "CA", "IN", "TI", "PO", "MA","SO" ]
    var AutoInc = ""
    var Running = ""
    var NumberingRunning = ""
    var create_date = new Date();
    if (DocumentnumberingDb) {
        if (document_numbering_yreset == true) {
            strAutoInc = module_id + document_numbering_prefixnumber
        } else {
            strAutoInc = module_id + document_numbering_prefixnumber + document_numbering_prefixyear
        }
        AutoInc = autoInc(strAutoInc)
        Running = addZeroLeft(document_numbering_running, AutoInc + parseInt(document_numbering_running));
        NumberingRunning = document_numbering_prefixnumber + prefixyear + Running

    } else {
        strAutoInc = module_id + NoConfig[module_id] + moment().format("YYMM")
        AutoInc = autoInc(strAutoInc)
        Running = addZeroLeft(3, AutoInc);
        NumberingRunning = NoConfig[module_id] + moment().format("YYMM") + Running
        console.log(NumberingRunning)

    }

    Documentrunningformat.insert({ module_id: module_id, company_id: company_id, branch_id: branch_id, document_running_format_number: NumberingRunning, document_running_format_id: autoInc("documentrunningformat"), create_date: create_date });
    
    return NumberingRunning;
}