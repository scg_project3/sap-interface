autoInc = function(name) {
    var result = Counters.findAndModify({
        query: { id: name },
        update: { $inc: { seq: 1 } },
        upsert: true,
        new: true
    });
    var seq = (result.seq) ? result.seq : ((result.value) ? result.value.seq : false)
    return seq;
}

addZeroLeft = function(cnt, numbertic) {
    var str = "" + numbertic
    var pad = "";
    for (i = 1; i <= cnt; i++) {
        pad += "0";
    }
    var ans = pad.substring(0, pad.length - str.length) + str
    return ans;
}

dateToArray = function(date) {
    var dateRetrun = [];
    var dateArray = date.split(",");
    _.each(dateArray, function(v, k, l) {
        var dateSplit = v.split("-")
        if (dateSplit.length > 1) {
            var dateOje = {
                start_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString(),
                end_date: moment(dateSplit[1], "DD/MM/YYYY H:mm").toISOString()
            };
        } else {
            var dateOje = {
                start_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString(),
                end_date: moment(dateSplit[0], "DD/MM/YYYY H:mm").toISOString()
            };
        }
        dateRetrun.push(dateOje)
    });
    return dateRetrun;
}


// updateAssetstoreavailable = function(data) {

//     // data.asset_class_id
//     // data.asset_type_id
//     // data.asset_sub_type_id
//     // data.asset_model_id

//     var qty = Assetstore.find({ asset_class_id: data.asset_class_id, asset_model_id: data.asset_model_id, asset_type_id: data.asset_type_id, asset_sub_type_id: data.asset_sub_type_id }).count();
//     console.log("qty",qty);
//     var agg = Assetstoretransaction.aggregate(
//         [
//             {
//                 $match: { asset_class_id: data.asset_class_id, asset_model_id: data.asset_model_id, asset_type_id: data.asset_type_id, asset_sub_type_id: data.asset_sub_type_id }
//             }, {
//                 $group: {
//                     _id: "$asset_store_transaction_status",
//                     totalQuantity: { $sum: { $multiply: ["$asset_store_transaction_select_quantity"] } },
//                     count: { $sum: 1 },
//                 }
//             }
//         ]
//     )

//     var avai = 0;
//     if (agg.length != 0) {
//      var new_arr = [];     
//          new_arr[1] = {totalQuantity:0};
//          new_arr[2] = {totalQuantity:0};
//         for (i = 0; i < agg.length; i++) { new_arr[agg[i]._id] = agg[i]; }
//         _.each(agg,function(v,k){
//          new_arr[k] = v;
//         })

//         avai = new_arr[1].totalQuantity - new_arr[2].totalQuantity; // IN - OUT

//     } else {
//         avai = qty
//     }

//     var a = Assetstoreavailable.update({ asset_class_id: data.asset_class_id, asset_model_id: data.asset_model_id, asset_type_id: data.asset_type_id, asset_sub_type_id: data.asset_sub_type_id }, { $set: { quantity: qty, available: avai } }, { upsert: true });
//     return a;
// }

updateAssetstoreavailable = function(data) { //total
    var qty = Assetstore.find({ asset_model_id: data.asset_model_id, asset_type_id: data.asset_type_id, asset_sub_type_id: data.asset_sub_type_id }).count();
    var a = Assetstoreavailable.update({ asset_model_id: data.asset_model_id, asset_type_id: data.asset_type_id, asset_sub_type_id: data.asset_sub_type_id }, { $set: { quantity: qty } }, { upsert: true });
    return a;
}

// InsertProductTransaction = function(date,qty){

//     //var date = "01/10/2016";
//     var data = {
//         // quo_entry_id: 37,
//         // quo_sub_id: 28,
//         // quo_sub_version_id: 47,
//         product_id: 2,
//         asset_class_id:14,
//         asset_model_id:1,
//         asset_type_id:14,
//         asset_sub_type_id:31,
//         booking_date:new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
//         booking_quantity:qty,
//         create_date:new Date()
//     };

//     Producttransaction.insert(data);
// }

InsertProductTransaction = function(data) {

    var date = "24/10/2016";
    var data = {
        quo_entry_id: 37,
        quo_sub_id: 28,
        quo_sub_version_id: 47,
        product_id: 2,
        asset_model_id: 2,
        asset_type_id: 1,
        asset_sub_type_id: 1,
        booking_date: new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
        booking_quantity: 1,
        create_date: new Date()
    };

    Producttransaction.insert(data);
}


//########################## CREW #############################

updateCrewSkillavailable = function(data) {
    var listcrewskillmap = Crewskillmap.find().fetch();

    var agg = Crewskillmap.aggregate(
        [{
                $group: {
                    _id: "$crew_skill_id",
                    count: { $sum: 1 },
                }
            }

        ]
    )
    _.each(agg, function(v, k, l){
    
        Crewskillavailable.update({ crew_skill_id: v._id }, { $set: { quantity: v.count } }, { upsert: true });
    
    });

    return "OK";
}


InsertCrewTransaction = function(data) {

    var date = "24/10/2016";
    var data = {
        crew_id: 1,
        crew_skill_id: 1,
        booking_date: new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
        booking_quantity: 1,
        create_date: new Date()
    };

    Crewordertransaction.insert(data);
}


InsertCrewSkillTransaction = function(data) {

    var date = "24/10/2016";
    var data = {
        crew_skill_id: 1,
        booking_date: new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
        booking_quantity: 1,
        create_date: new Date()
    };

    Crewskilltransaction.insert(data);
}



updateMaterialAvailable = function(data){

     var data = {};
     data.asset_store_id = 1;
     data.quantity = 10;

     var a = Materialavailable.update({ asset_store_id:data.asset_store_id }, { $set: { quantity: data.quantity } }, { upsert: true });
   
}

updateMaterialAvailableSeq = function(asset_store_id,asset_store_transaction_quantity,type){

     // var data = {};
     // data.asset_store_id = 1;
     // data.quantity = 10;
          var quantity = 0;
          var m = Assetstoretransaction.find({asset_store_id:parseInt(asset_store_id)}).fetch();
        _.each(m,function(v,k){
                var type = Warehousemovementtype.findOne({warehouse_movement_type_id:v.warehouse_movement_type_id});
                if(type.warehouse_movement_type_indicator_id == 1){
                    quantity = quantity + v.asset_store_transaction_quantity;
                }
                else if(type.warehouse_movement_type_indicator_id == 2){
                    quantity =  quantity - v.asset_store_transaction_quantity;
                }

        })
    
       var a = Materialavailable.update({ asset_store_id:asset_store_id }, { $set: { quantity: quantity } }, { upsert: true });



    
   
}


InsertProductmaterialtransaction = function(data) {

        // อย่าลืม x กับ productmap material นะจ้ะ

    var date = "24/10/2016";

    var data = {
        asset_store_id: 1,
        booking_date: new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
        booking_quantity: 10,
        create_date: new Date()
    };

    Productmaterialtransaction.insert(data);
}


InsertMaterialordertransaction = function(data) {

     // อย่าลืม x กับ productmap material นะจ้ะ
        
        
    var date = "24/10/2016";
    
    var data = {
        asset_store_id: 1,
        booking_date: new Date(moment(date, "DD/MM/YYYY H:mm").toISOString()),
        booking_quantity: 10,
        create_date: new Date()
    };

    Materialordertransaction.insert(data);
}



// Phatja

arrayToDate = function(arrayDate) {
    var dateReturn = "";
    console.log(arrayDate)
    _.each(arrayDate, function(v, k, l) {
        var start_date = moment(v.start_date).format('DD/MM/YYYY')
        var end_date = moment(v.end_date).format('DD/MM/YYYY')
        if (start_date == end_date) {
            dateReturn += start_date + ","
        } else {
            dateReturn += start_date + "-" + end_date + ","
        }
    });
    dateReturn = dateReturn.substring(0, dateReturn.length - 1);
    return dateReturn;
}



sortShootingDate = function(datadate) {
    var array_date = datadate.replace(/[\s,]+/g, ',').split(",");
    var new_arrdate = [];
    _.each(array_date, function(v, k) {
        var _v = v.replace(/[\s,]+/g, '-').split("-");
        var start_date = _v[0];
        var stop_date = _v[0];
        if (_v.length == 2) {
            stop_date = _v[1];
        }
        new_arrdate.push({
            start_date: new Date(moment(start_date, "DD/MM/YYYY H:mm").toISOString()),
            end_date: new Date(moment(stop_date, "DD/MM/YYYY H:mm").toISOString()),
        })
    });
    var dateReturn = [];
    _.each(new_arrdate, function(v2, k2, l2) {
        var start_date = v2.start_date
        var end_date = v2.end_date
        var dateInc = moment(start_date)
        var diffDate = moment(end_date).diff(moment(start_date), "days") + 1

        for (var i = 0; i < diffDate; i++) {
            dateReturn.push({
                dateiso: new Date(dateInc.toISOString()),
                date: dateInc.format('DD/MM/YYYY')
            });
            dateInc = dateInc.add(1, "day")
        }
    });

    var sortDate = dateReturn.sort(function(a, b) {
        a = moment(a.date, "DD/MM/YYYY H:mm")
        b = moment(b.date, "DD/MM/YYYY H:mm")
        return new Date(a._d).getTime() - new Date(b._d).getTime()
    });

    var alldate = {};
    alldate.datestr = [];
    alldate.dateiso = [];
    _.each(sortDate, function(v, k) {
        alldate.datestr.push(v.date);
        alldate.dateiso.push(v.dateiso)
    });
    return alldate;

}

calUpdateInvoice = function(cal_id,invoice_id,invoice_tag){
    var data = {
        invoice_id: invoice_id,
        invoice_tag: invoice_tag,
        invoice_status: 2
    }
    Calculationnote.update({cal_id:cal_id}, {$set:data});
}

calCancelInvoice = function(cal_id){
    var data = {
        invoice_status: 4
    }
    Calculationnote.update({cal_id:cal_id}, {$set:data});
}