var bpmURL = "http://192.168.1.77:8080/engine-rest/";
var processName = "JS_JE_EX_01";
var processDefinitionId = "JS_JE_EX_01:1:2df4be30-e156-11e6-a0b7-000c2950a12e";

Meteor.methods({
    bpmStart: function(data) {
        this.unblock();
        var variables = {}
        variables["variables"] = {}
        _.each(data, function(v, k, l) {
            variables["variables"][v.key] = {
                value: v.value,
                type: "String"
            }
        });

        try {
            var result = Meteor.http.post(bpmURL + "process-definition/key/" + processName + "/start/", {
                data: variables
            });
            return result.data.id; //process instances id
        } catch (error) {
            return 'false';
        }
    },
    bpmGetExecution: function(task_id, instances_id) { //not work in loop parallel
        this.unblock();
        try {
            var result = Meteor.http.get(bpmURL + "task?processDefinitionId=" + processDefinitionId + "&taskDefinitionKey=" + task_id + "&processInstanceId=" + instances_id);
            if (result.data.length > 0) {
                return result.data[0].executionId; // execution id
            } else {
                return 'false'
            }
        } catch (error) {
            return 'false';
        }
    },
    bpmSubmit: function(task_id, execution_id, conData) {
        this.unblock();

        console.log(task_id, execution_id, conData);

        var variables = {}
        variables["variables"] = {}
        _.each(conData, function(v, k, l) {
            variables["variables"][v.key] = {
                value: v.value,
                type: "String"
            }
        });

        try {
            var result = Meteor.http.get(bpmURL + "task?processDefinitionId=" + processDefinitionId + "&taskDefinitionKey=" + task_id + "&executionId=" + execution_id);
            if (result.data.length > 0) {
                var result = Meteor.http.post(bpmURL + "task/" + result.data[0].id + "/complete/", {
                    data: variables
                });
                return 'true'
            } else {
                return 'false'
            }
        } catch (error) {
            return 'false';
        }
    },
    bpmGetExecutionInTask: function(task_id) {
        this.unblock();
        try {
            var result = Meteor.http.get(bpmURL + "task?taskDefinitionKey=" + task_id + "&active=true&processDefinitionId=" + processDefinitionId);
            var executionId = []
            _.each(result.data, function(v, k, l) {
                executionId.push(v.executionId);
            });
            return executionId;
        } catch (error) {
            return 'false';
        }
    },
    bpmGetExecutionByVariables: function(variables, value) { // variables in task
        this.unblock();
        try {
            var result = Meteor.http.get(bpmURL + "execution?variables=" + variables + "_eq_" + value);
            if (result.data.length > 0) {
                return result.data[0].id; // execution id
            } else {
                return 'false'
            }
        } catch (error) {
            return 'false';
        }
    },
    bpmSetExcutionVariables: function(execution_id, data) {
        this.unblock();
        var variables = {}
        variables["modifications"] = {}
        _.each(data, function(v, k, l) {
            variables["modifications"][v.key] = {
                value: v.value,
                type: "String"
            }
        });

        try {
            var result = Meteor.http.post(bpmURL + "execution/" + execution_id + "/localVariables", {
                data: variables
            });
            return "true"
        } catch (error) {
            return 'false';
        }
    }
})
